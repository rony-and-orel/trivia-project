#include "LoginManager.h"

//C'tor
LoginManager::LoginManager(IDatabase* database) :
	m_database(database), m_loggedUsers()
{}

/*
tries to signup a new user.
input: user's username, password and email.
output: if operation was successful.
*/
bool LoginManager::signup(const std::string& username, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birthDate)
{
	const std::regex passRegex("^(?=.*[A-Z])(?=.*[a-z])(?=^\\D*\\d\\D*$)(?=^[^!@#$%^&*]*[!@#$%^&*][^!@#$%^&*]*$)(^.{8}$).*$"), emailRegex("^[a-zA-Z0-9.]+@(\\w[a-zA-Z0-9.]+\\w)$"), addressRegex("^\\([aA-zZ]+,\\d+,[aA-zZ]+\\)$"), phoneRegex("^0\\d{1,2}-\\d{7}$"), dateRegex("^\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d$|^\\d\\d\\/\\d\\d\\/\\d\\d\\d\\d$");
	bool flag = false;
	if (!this->m_database->doesUserExist(username) /*&& std::regex_match(password, passRegex) && std::regex_match(email, emailRegex) //checks that all of the regexes for the fields are valid
		&& std::regex_match(address, addressRegex) && std::regex_match(phone, phoneRegex) && std::regex_match(birthDate, dateRegex)*/)
	{
		this->m_database->addNewUser(username, password, email, address, phone, birthDate);
		flag = true;
	}
	return flag;
}

/*
tries to login a user.
input: user's username and password.
output: if operation was successful.
*/
bool LoginManager::login(const std::string& username, const std::string& password)
{
	bool success = false;
	std::unique_lock<std::mutex> lck(m_mutex);
	if (this->m_database->doesUserExist(username) && this->m_database->doesPasswordMatch(username, password))
	{
		this->m_loggedUsers.emplace(username);
		success = true;
	}
	return success;
}

/*
tries to logout a user.
input: user's username.
output: if operation was successful.
*/
bool LoginManager::logout(const std::string& username)
{
	std::unique_lock<std::mutex> lck(m_mutex);
	bool flag = false;
	auto it = std::find(this->m_loggedUsers.begin(), this->m_loggedUsers.end(), username);
	if (it != this->m_loggedUsers.end())
	{
		this->m_loggedUsers.erase(it);
		flag = true;
	}
	return flag;
}

/*
checks if the user is logged in.
input: username.
output: if logged in.
*/
bool LoginManager::isLoggedIn(const std::string& username) const
{
	return std::find(this->m_loggedUsers.begin(), this->m_loggedUsers.end(), username) != this->m_loggedUsers.end();
}

/*
checks if the user is signed up
input: username.
output: if logged in.
*/
bool LoginManager::doesExist(const std::string& username) const
{
	return m_database->doesUserExist(username);
}