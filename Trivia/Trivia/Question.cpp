#include "Question.h"


//C'tor
//note for use: The corrected answer needs to be in answers vector when constructing, it won't be added in constructor.
Question::Question(const std::string& question, const std::array<std::string, THREE>& wrongAnswers, const std::string& correctAnswer) :
	m_question(question), m_answers({ wrongAnswers[0], wrongAnswers[1], wrongAnswers[TWO], correctAnswer }), m_correctAnswer(0)
{
	std::shuffle(m_answers.begin(), m_answers.end(), std::default_random_engine((unsigned int)std::chrono::system_clock::now().time_since_epoch().count()));

	while (m_correctAnswer < FOUR && m_answers[m_correctAnswer] != correctAnswer)
	{
		m_correctAnswer++;
	}
}

//returns the question
std::string Question::getQuestion() const
{
	return this->m_question;
}

//returns array of all possible answers, in random order
std::array<std::string, FOUR> Question::getPossibleAnswers() const
{
	return m_answers;
}

//returns the correct answer
std::string Question::getCorrectAnswer() const
{
	return this->m_answers[m_correctAnswer];
}

//returns the correct answer
unsigned short Question::getCorrectAnswerId() const
{
	return m_correctAnswer;
}

/*
Checks if the user answered correctly.
input: answer try.
output: if correct.
*/
bool Question::isAnswerCorrect(unsigned short id) const
{
	return id == m_correctAnswer;
}
