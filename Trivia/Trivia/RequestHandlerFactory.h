#pragma once
#include <string>
#include "IDataBase.h"
#include "SqliteDatabase.h"
#include "MongoDatabase.h"
#include "Definitions.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomRequestHandler.h"
#include "GameRequestHandler.h"

using std::string;

class LoginRequestHandler;
class MenuRequestHandler;
class RoomRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
private:
	IDatabase* m_database;
	LoginManager m_loginManager;
	RoomManager m_roomManager;
	StatisticsManager m_statisticsManager;
public:
	RequestHandlerFactory();
	~RequestHandlerFactory();
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(const LoggedUser& user);
	MenuRequestHandler* createMenuRequestHandler(LoggedUser&& user);
	RoomRequestHandler* createRoomRequestHandler(unsigned int roomid, const LoggedUser& user);
	RoomRequestHandler* createRoomRequestHandler(unsigned int roomid, LoggedUser&& user);
	GameRequestHandler* createGameRequestHandler(Room& room, const LoggedUser& user, unsigned int currQuestion = 0);
	GameRequestHandler* createGameRequestHandler(Room& room, LoggedUser&& user, unsigned int currQuestion = 0);
	LoginManager& getLoginManager();
	RoomManager& getRoomManager();
	StatisticsManager& getStatisticsManager();
};

