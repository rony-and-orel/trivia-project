#include "Game.h"

//advances the question
void Game::advanceQuestion()
{
	killTimer();
	m_questions.pop();
	m_metadata.currQuestionid++;
	m_timeStarted = false;
	for (auto& player : m_players) //restore everyone's status to 'not yet answered question'
	{
		player.second.answeredQuestion = false;
	}
	startTimer();
}

//timeout thread for auto-advancing the question
void Game::timer()
{
	unsigned short id = m_metadata.currQuestionid;
	std::this_thread::sleep_for(std::chrono::seconds(m_metadata.timeout + 1));
	if (id == m_metadata.currQuestionid)
	{
		std::lock_guard<std::mutex> lck(m_mutex);
		if (!m_questions.empty())
		{
			advanceQuestion();
		}
	}
}

//kill question timer
void Game::killTimer()
{
	if (m_timer != nullptr)
	{
		delete m_timer;
		m_timer = nullptr;
	}
}

//start question timer
void Game::startTimer()
{
	m_timer = new std::thread(&Game::timer, this);
	m_timer->detach();
}

//C'tor
Game::Game(const std::queue<Question>& questions, const std::unordered_set<LoggedUser>& users, unsigned short timePerQuestion) :
	m_metadata({timePerQuestion, questions.size(), 1}), m_playerCloseCount(users.size()), m_questions(questions), m_timeStarted(false), m_timer(nullptr)
{
	m_players.reserve(users.size());
	for (auto& user : users)
	{
		m_players.emplace(std::piecewise_construct, std::forward_as_tuple(user), std::forward_as_tuple());
	}

	startTimer();
}

//D'tor
Game::~Game()
{
	killTimer();
}

/*
returns the current question to be answered.
input: user.
output: question id, question.
*/
const std::pair<unsigned short, const Question&> Game::getQuestion() const
{
	std::lock_guard<std::mutex> lck(m_mutex);
	if (!m_timeStarted)
	{
		m_startTime = std::chrono::high_resolution_clock::now();
		m_timeStarted = true;
	}
	return { m_metadata.currQuestionid, m_questions.front() };
}

//returns if there are no more questions
bool Game::isGameOver() const
{
	return m_questions.empty();
}

/*
checks if the user has answeres correctly and updates data accordingly
input: answer id, in the question (not an absolute id across all questions in all games)
output: if correct.
*/
bool Game::submitAnswer(unsigned short id, const LoggedUser& user)
{
	bool flag = false;
	std::unique_lock<std::mutex> lck(m_mutex);
	if (!m_players[user].answeredQuestion)
	{
		m_players[user].answeredQuestion = true;
		m_players[user].totalAnswerTime += (unsigned int)std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - m_startTime).count(); //update player time

		flag = m_questions.front().isAnswerCorrect(id);

		if (flag)
		{
			m_players[user].correctAnswerCount++;
		}
		else
		{
			m_players[user].wrongAnswerCount++;
		}

		//if all players have answered the current question, advance to next question
		if (std::find_if(m_players.begin(), m_players.end(), [](const std::pair<const LoggedUser, GameStatistics>& u) -> bool {return !u.second.answeredQuestion; }) == m_players.end())
		{
			advanceQuestion();
		}
	}
	return flag;
}

/*
returns the amount of players left in game (not including calling player). after a player has called this function, it is considered as not in game.
input: none.
output: amount of players left.
*/
unsigned short Game::getPlayersLeft()
{
	std::lock_guard<std::mutex> lck(m_mutex);
	return --m_playerCloseCount;
}

/*
gets time since start of question.
input: none.
output: time since question start, in milliseconds;
*/
unsigned short Game::getTimeSinceStart() const
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - m_startTime).count();
}

//getter for game data
const GameData& Game::getGameData() const
{
	return m_metadata;
}

//returns the players in room
const std::unordered_map<LoggedUser, GameStatistics>& Game::getPlayers() const
{
	return m_players;
}
