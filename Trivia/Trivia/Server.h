#pragma once

#include "Communicator.h"
#include <iostream>
#include <thread>
#include <exception>
#include <iostream>
#include "Communicator.h"

class Server
{
public:
	Server();
	Server(const Server&) = delete;
	~Server();
	void run();

	using console_function_handler = void (Server::*)(void); //server's console command function handler
private:
	Communicator m_communicator;
	static const std::map<std::string, console_function_handler> _console_commands;

	void paint(const std::string& paint);
	void shutdown();
	void cls();
	void help();
	void moo();
};

