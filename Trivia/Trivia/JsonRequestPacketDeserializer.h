#pragma once
#include <nlohmann/json.hpp>
#include "Definitions.h"

class JsonRequestPacketDeserializer
{
public:
	static request::Signup deserializeSignupRequest(const std::vector<byte>& Buffer);
	static request::Login deserializeLoginRequest(const std::vector<byte>& Buffer);
	static request::CreateRoom deserializeCreateRoomRequest(const std::vector<byte>& Buffer);
	static request::JoinRoom deserializeJoinRoomRequest(const std::vector<byte>& Buffer);
	static request::Promote deserializePromoteRequest(const std::vector<byte>& Buffer);
	static request::Demote deserializeDemoteRequest(const std::vector<byte>& Buffer);
	static request::Invite deserializeInviteRequest(const std::vector<byte>& Buffer);
	static request::Uninvite deserializeUninviteRequest(const std::vector<byte>& Buffer);
	static request::SubmitAnswer deserializeSubmitAnswerRequest(const std::vector<byte>& Buffer);
	static request::StartGame deserializeStartGameRequest(const std::vector<byte>& Buffer);
};
