#pragma once
#include <vector>
#include <ctime>
#include "Definitions.h"

using std::vector;

struct RequestResult;
struct RequestInfo;

class IRequestHandler
{
public:
	IRequestHandler() = default;
	~IRequestHandler() = default;

	virtual bool isRequestRelevant(const MessageCode code) = 0;
	virtual RequestResult handleRequest(const RequestInfo& info) = 0;
	virtual void disconnect() = 0;
};

typedef struct RequestResult
{
	vector<byte> response;
	IRequestHandler* newHandler;
}RequestResult;

typedef struct RequestInfo
{
	vector<byte> buffer;
}RequestInfo;

