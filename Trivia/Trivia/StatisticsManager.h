#pragma once
#include <unordered_map>
#include "IDatabase.h"
#include "Game.h"
#include "LoggedUser.h"
#include "Definitions.h"

class StatisticsManager
{
private:
	IDatabase* m_database;
public:
	explicit StatisticsManager(IDatabase* database);
	StatisticsManager(const StatisticsManager&) = delete;
	~StatisticsManager() = default;
	std::vector<std::string> getStatistics(const LoggedUser& user) const;
	void updateStatistics(const Game& game);
};
