#pragma once
#include <string>
#include <queue>
#include <vector>
#include "Question.h"
#include "Definitions.h"
#include "Protector.h"

class IDatabase
{
public:
	virtual bool doesUserExist(const std::string& username) const = 0;
	virtual bool doesPasswordMatch(const std::string& username, const std::string& password) const = 0;
	virtual void addNewUser(const std::string& username, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birthDate) = 0;

	virtual std::vector<std::string> getCategories() const = 0;
	virtual std::queue<Question> getQuestions(unsigned short count, byte diff, const std::string& category = "") const = 0;

	virtual float getPlayerAverageAnswerTime(const std::string& username) const = 0;
	virtual void incrementPlayerTotalAnswerTime(const std::string& username, unsigned int answerTime) = 0;

	virtual unsigned int getNumOfCorrectAnswers(const std::string& username) const = 0;
	virtual void incrementNumOfCorrectAnswers(const std::string& username, unsigned int correctAnswerCount) = 0;

	virtual unsigned int getNumOfWrongAnswers(const std::string& username) const = 0;
	virtual void incrementNumOfWrongAnswers(const std::string& username, unsigned int wrongAnswerCount) = 0;

	virtual unsigned int getNumOfPlayerGames(const std::string& username) const = 0;
	virtual void incrementNumOfPlayerGames(const std::string& username) = 0;
};
