#include "LoginRequestHandler.h"

//C'tor
LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* handleFactory) :
	IRequestHandler(), m_handlerFactory(handleFactory)
{}

/*
check if request is relevant.
input: message code.
output: true/false statement.
*/
bool LoginRequestHandler::isRequestRelevant(const MessageCode code)
{
	return code == MessageCode::LOGIN || code == MessageCode::SIGNUP;
}

/*
handles the request.
input: request information.
output: request result.
*/
RequestResult LoginRequestHandler::handleRequest(const RequestInfo& info)
{
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	if (info.buffer[0] == (byte)MessageCode::SIGNUP) //if the request is signup request
	{
		result = signup(info);
	}
	else if(info.buffer[0] == (byte)MessageCode::LOGIN) //if the request is login request
	{
		result = login(info);
	}

	return result;
}

void LoginRequestHandler::disconnect(){}

/*
sign up operation.
input: request information.
output: results.
*/
RequestResult LoginRequestHandler::signup(const RequestInfo& info)
{
	response::Login response = {
		STATUS_FAIL
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	request::Signup signupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(info.buffer);
	
	//try to signup and login
	if (Protector::isSafe(signupRequest.username) && Protector::isSafe(signupRequest.password) && Protector::isSafe(signupRequest.email) && Protector::isSafe(signupRequest.address) && Protector::isSafe(signupRequest.phoneNumber) && Protector::isSafe(signupRequest.birthDate)
		&& m_handlerFactory->getLoginManager().signup(signupRequest.username, signupRequest.password, signupRequest.email, signupRequest.address, signupRequest.phoneNumber, signupRequest.birthDate) 
		&& m_handlerFactory->getLoginManager().login(signupRequest.username, signupRequest.password))
	{
		response.status = STATUS_SUCCESS; //if succeeded
		result.newHandler = m_handlerFactory->createMenuRequestHandler(std::move(LoggedUser(signupRequest.username)));
	}
	else
	{
		result.newHandler = m_handlerFactory->createLoginRequestHandler();
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

/*
login operation.
input: request information.
output: results.
*/
RequestResult LoginRequestHandler::login(const RequestInfo& info)
{
	response::Login response = {
		STATUS_FAIL
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	request::Login loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(info.buffer);

	if (Protector::isSafe(loginRequest.username) && Protector::isSafe(loginRequest.password)
		&& m_handlerFactory->getLoginManager().login(loginRequest.username, loginRequest.password)) //try to login
	{
		response.status = STATUS_SUCCESS; //if succeeded
		result.newHandler = m_handlerFactory->createMenuRequestHandler(std::move(LoggedUser(loginRequest.username)));
	}
	else
	{
		result.newHandler = m_handlerFactory->createLoginRequestHandler();
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}