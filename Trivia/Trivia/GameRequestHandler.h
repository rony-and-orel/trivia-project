#pragma once
#include <string>
#include <iterator>
#include <algorithm>
#include "RequestHandlerFactory.h"

using std::string;

class RequestHandlerFactory;

class GameRequestHandler : public IRequestHandler
{
private:
	Room& m_room;
	LoggedUser m_user;
	unsigned short m_currQuestion;
	RequestHandlerFactory* m_handlerFactory;

	RequestResult getQuestion(const RequestInfo& info);
	RequestResult getGameResults(const RequestInfo& info);
	RequestResult submitAnswer(const RequestInfo& info);
	RequestResult leaveGame(const RequestInfo& info);
	RequestResult getGameInfo(const RequestInfo& info);
public:
	GameRequestHandler(Room& room, const LoggedUser& user, unsigned short currQuestion, RequestHandlerFactory* handlerFactory);
	GameRequestHandler(Room& room, LoggedUser&& user, unsigned short currQuestion, RequestHandlerFactory* handlerFactory);
	~GameRequestHandler() = default;
	bool isRequestRelevant(const MessageCode code) override;
	RequestResult handleRequest(const RequestInfo& info) override;
	void disconnect() override;
};

