#include "JsonRequestPacketDeserializer.h"

/*
Deserializes a Signup request message.
	json fields: "username" - string; "password" - string; "email" - string; "address" - string; "phone_number" - string; "birth_date" - string.
input: message buffer.
output: SignupRequest struct.
*/
request::Signup JsonRequestPacketDeserializer::deserializeSignupRequest(const std::vector<byte>& Buffer)
{
	nlohmann::json j = nlohmann::json::parse(std::string(Buffer.begin() + HEADER_SIZE, Buffer.end()));
	return { j["username"].get<std::string>(), j["password"].get<std::string>(), j["email"].get<std::string>(), j["address"].get<std::string>(), j["phone_number"].get<std::string>(), j["birth_date"].get<std::string>() };
}

/*
Deserializes a Login request message.
	json fields: "username" - string; "password" - string.
input: message buffer.
output: LoginRequest struct.
*/
request::Login JsonRequestPacketDeserializer::deserializeLoginRequest(const std::vector<byte>& Buffer)
{
	nlohmann::json j = nlohmann::json::parse(std::string(Buffer.begin() + HEADER_SIZE, Buffer.end()));
	return { j["username"].get<std::string>() , j["password"].get<std::string>() };
}

/*
Deserializes a CreateRoom request message.
	json fields: "name" - string; "max_users" - unsigned int; "is_public" - 1 or 0.
input: message buffer.
output: CreateRoom struct.
*/
request::CreateRoom JsonRequestPacketDeserializer::deserializeCreateRoomRequest(const std::vector<byte>& Buffer)
{
	nlohmann::json j = nlohmann::json::parse(std::string(Buffer.begin() + HEADER_SIZE, Buffer.end()));
	return { j["name"].get<std::string>() , j["max_users"].get<unsigned short>() , j["is_public"].get<byte>() ? true : false};
}

/*
Deserializes a JoinRoom request message.
	json fields: "id" - unsigned int.
input: message buffer.
output: JoinRoom struct.
*/
request::JoinRoom JsonRequestPacketDeserializer::deserializeJoinRoomRequest(const std::vector<byte>& Buffer)
{
	nlohmann::json j = nlohmann::json::parse(std::string(Buffer.begin() + HEADER_SIZE, Buffer.end()));
	return { j["id"].get<unsigned int>() };
}


/*
Deserializes a Promote request message.
	json fields: "user" - string.
input: message buffer.
output: Promote struct.
*/
request::Promote JsonRequestPacketDeserializer::deserializePromoteRequest(const std::vector<byte>& Buffer)
{
	nlohmann::json j = nlohmann::json::parse(std::string(Buffer.begin() + HEADER_SIZE, Buffer.end()));
	return { j["user"].get<std::string>() };
}

/*
Deserializes a Demote request message.
	json fields: "user" - string.
input: message buffer.
output: Demote struct.
*/
request::Demote JsonRequestPacketDeserializer::deserializeDemoteRequest(const std::vector<byte>& Buffer)
{
	nlohmann::json j = nlohmann::json::parse(std::string(Buffer.begin() + HEADER_SIZE, Buffer.end()));
	return { j["user"].get<std::string>() };
}

/*
Deserializes an Invite request message.
	json fields: "user" - string.
input: message buffer.
output: Invite struct.
*/
request::Invite JsonRequestPacketDeserializer::deserializeInviteRequest(const std::vector<byte>& Buffer)
{
	nlohmann::json j = nlohmann::json::parse(std::string(Buffer.begin() + HEADER_SIZE, Buffer.end()));
	return { j["user"].get<std::string>() };
}

/*
Deserializes an Uninvite request message.
	json fields: "user" - string.
input: message buffer.
output: Uninvite struct.
*/
request::Uninvite JsonRequestPacketDeserializer::deserializeUninviteRequest(const std::vector<byte>& Buffer)
{
	nlohmann::json j = nlohmann::json::parse(std::string(Buffer.begin() + HEADER_SIZE, Buffer.end()));
	return { j["user"].get<std::string>() };
}

/*
Deserializes a SubmitAnswer request message.
	json fields: "question_id" - unsigned short; "answer_id" - unsigned short.
input: message buffer.
output: SubmitAnswer struct.
*/
request::SubmitAnswer JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(const std::vector<byte>& Buffer)
{
	nlohmann::json j = nlohmann::json::parse(std::string(Buffer.begin() + HEADER_SIZE, Buffer.end()));
	return { j["answer_id"].get<unsigned short>() };
}

/*
Deserializes a StartGame request message.
	json fields: "time_per_question" - unsigned short; "round_count" - unsigned short; "diff" - byte; "category" - string.
input: message buffer.
output: StartGame struct.
*/
request::StartGame JsonRequestPacketDeserializer::deserializeStartGameRequest(const std::vector<byte>& Buffer)
{
	nlohmann::json j = nlohmann::json::parse(std::string(Buffer.begin() + HEADER_SIZE, Buffer.end()));
	return { j["time_per_question"].get<unsigned short>(), j["round_count"].get<unsigned short>(), j["diff"].get<byte>(), j["category"].get<std::string>() };
}
