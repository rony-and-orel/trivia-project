#pragma once
#include <chrono>
#include <string>
#include <vector>
#include <memory>
#include <openssl/sha.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include "Definitions.h"

class Protector
{
private:
	struct FreeBIO
	{
		void operator()(BIO* p)
		{
			BIO_free_all(p);
		}
	};

	static std::string generateSalt();
	static std::vector<byte> generateHash(const std::string& password, const std::string& salt);
public:
	struct Hash
	{
		std::vector<byte> digest;
		std::string salt;
	};

	//hash related functions
	static Protector::Hash generateNewHash(const std::string& password);
	static bool compareHash(const std::string& password, const std::string& salt, const std::vector<byte>& hash);
	static std::string toStr(const std::vector<byte>& binary);
	static std::vector<byte> toBinary(const std::string& encoded);

	//SQL injection protection
	static bool isSafe(const std::string& str);
};

