#include "GameRequestHandler.h"

//C'tor
GameRequestHandler::GameRequestHandler(Room& room, const LoggedUser& user, unsigned short currQuestion, RequestHandlerFactory* handlerFactory) :
	m_room(room), m_user(user), m_currQuestion(currQuestion), m_handlerFactory(handlerFactory)
{}
GameRequestHandler::GameRequestHandler(Room& room, LoggedUser&& user, unsigned short currQuestion, RequestHandlerFactory* handlerFactory) :
	m_room(room), m_user(std::move(user)), m_currQuestion(currQuestion), m_handlerFactory(handlerFactory)
{}

/*
check if request is relevant.
input: message code.
output: true/false statement.
*/
bool GameRequestHandler::isRequestRelevant(const MessageCode code)
{
	return (code == MessageCode::GET_QUESTION || code == MessageCode::GET_GAME_RESULTS || code == MessageCode::SUBMIT_ANSWER ||
		code == MessageCode::GET_GAME_INFO || code == MessageCode::LEAVE_GAME) &&
		m_handlerFactory->getLoginManager().isLoggedIn(m_user.getUsername());
}

/*
handles the request.
input: request information.
output: request result.
*/
RequestResult GameRequestHandler::handleRequest(const RequestInfo& info)
{
	RequestResult result = {
	vector<byte>(),
	nullptr
	};

	switch ((MessageCode)info.buffer[0])
	{
	case MessageCode::GET_QUESTION:
		result = getQuestion(info);
		break;
	case MessageCode::GET_GAME_RESULTS:
		result = getGameResults(info);
		break;
	case MessageCode::SUBMIT_ANSWER:
		result = submitAnswer(info);
		break;
	case MessageCode::LEAVE_GAME:
		result = leaveGame(info);
		break;
	case MessageCode::GET_GAME_INFO:
		result = getGameInfo(info);
		break;
	}
	return result;
}

void GameRequestHandler::disconnect()
{
	if (m_room.getRoomData().curPlayers == 1) //if last player in game, delete game and room
	{
		m_handlerFactory->getRoomManager().deleteGame(m_room);
		m_handlerFactory->getRoomManager().remove(m_room.getRoomData().id);
	}
	else
	{
		m_room.remove(m_user); //leave
	}
}

/*
handles the get question request.
input: request information.
output: request result.
*/
RequestResult GameRequestHandler::getQuestion(const RequestInfo& info)
{
	response::getQuestion responseQuestion = {
		STATUS_FAIL,
		"",
		0,
		0,
		std::array<std::string, FOUR>()
	};

	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	if(!m_room->isGameOver()) //if game isn't over, return next question
	{
		auto question = m_room->getQuestion();
		if (m_currQuestion != question.first)
		{
			m_currQuestion = question.first;

			responseQuestion.status = STATUS_SUCCESS;
			responseQuestion.question = question.second.getQuestion();
			responseQuestion.id = question.first;
			responseQuestion.time = m_room->getTimeSinceStart();
			responseQuestion.answers = question.second.getPossibleAnswers();
		}
	}
	result.response = JsonResponsePacketSerializer::serializeResponse(responseQuestion);
	result.newHandler = m_handlerFactory->createGameRequestHandler(m_room, std::move(m_user), m_currQuestion);
	return result;
}

RequestResult GameRequestHandler::getGameResults(const RequestInfo& info)
{
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	response::GetGameResults response = {
		STATUS_FAIL,
		std::vector<PlayerResults>()
	};

	if (m_room.isGameRunning() && m_room->isGameOver()) //if game is still running and is over
	{
		m_handlerFactory->getRoomManager().deleteGame(m_room); //delete the game
	}
	
	if (!m_room.isGameRunning())
	{
		response.results = m_room.getResultsFromLastGame(); //get the results

		response.status = STATUS_SUCCESS;
		result.newHandler = m_handlerFactory->createRoomRequestHandler(m_room.getRoomData().id, std::move(m_user)); //go back to room
	}
	else if(m_room.isGameRunning() && !m_room->isGameOver())
	{
		result.newHandler = m_handlerFactory->createGameRequestHandler(m_room, std::move(m_user), m_currQuestion); //retrieve to game
	}
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

/*
handles the submit answer request.
input: request information.
output: request result.
*/
RequestResult GameRequestHandler::submitAnswer(const RequestInfo& info)
{
	response::SubmitAnswer response = {
		STATUS_FAIL,
		0
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};
	request::SubmitAnswer request = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(info.buffer);

	if (!m_room->isGameOver())
	{
		response.correctAnswerId = m_room->getQuestion().second.getCorrectAnswerId();
		response.status = m_room->submitAnswer(request.answerid, m_user) ? STATUS_SUCCESS : STATUS_FAIL; //submit
	}
	
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createGameRequestHandler(m_room, std::move(m_user), m_currQuestion);

	return result;
}

/*
handles the leave game request.
input: request information.
output: request result.
*/
RequestResult GameRequestHandler::leaveGame(const RequestInfo& info)
{
	response::LeaveGame response = {
		STATUS_SUCCESS
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	if (m_room.getRoomData().curPlayers == 1) //if last player in game, delete game and room
	{
		m_handlerFactory->getRoomManager().deleteGame(m_room);
		m_handlerFactory->getRoomManager().remove(m_room.getRoomData().id);
	}
	else
	{
		m_room.remove(m_user); //leave
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createMenuRequestHandler(std::move(m_user));

	return result;
}

/*
handles the get game info request.
input: request information.
output: request result.
*/
RequestResult GameRequestHandler::getGameInfo(const RequestInfo& info)
{
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	response::GetGameInfo response = { m_room->getGameData() };

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createGameRequestHandler(m_room, std::move(m_user), m_currQuestion);
	return result;
}