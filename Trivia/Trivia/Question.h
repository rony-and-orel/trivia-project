#pragma once
#include <string>
#include <vector>
#include <array>
#include <chrono>
#include <random>

#define FOUR 4
#define THREE 3
#define TWO 2

class Question
{
private:
	std::string m_question;
	std::array<std::string, FOUR> m_answers;
	unsigned short m_correctAnswer;
public:
	explicit Question(const std::string& question, const std::array<std::string, THREE>& wrongAnswers, const std::string& correctAnswer);
	~Question() = default;
	std::string getQuestion() const;
	std::array<std::string, FOUR> getPossibleAnswers() const;
	std::string getCorrectAnswer() const;
	unsigned short getCorrectAnswerId() const;
	bool isAnswerCorrect(unsigned short id) const;
};

