#include "MenuRequestHandler.h"

//C'tor
MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory* handlerFactory, const LoggedUser& user) :
	IRequestHandler(), m_user(user), m_handlerFactory(handlerFactory)
{}
MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory* handlerFactory, LoggedUser&& user) :
	IRequestHandler(), m_user(std::move(user)), m_handlerFactory(handlerFactory)
{}

/*
check if request is relevant.
input: message code.
output: true/false statement.
*/
bool MenuRequestHandler::isRequestRelevant(const MessageCode code)
{
	return (code == MessageCode::GET_ROOMS || code == MessageCode::GET_STATISTICS ||
		code == MessageCode::JOIN_ROOM || code == MessageCode::CREATE_ROOM || code == MessageCode::LOGOUT) &&
		m_handlerFactory->getLoginManager().isLoggedIn(m_user.getUsername());
}

/*
handles the request.
input: request information.
output: request result.
*/
RequestResult MenuRequestHandler::handleRequest(const RequestInfo& info)
{
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	switch ((MessageCode)info.buffer[0]) //switch between request types
	{
		case MessageCode::GET_ROOMS:
			result = getRooms(info);
			break;
		case MessageCode::GET_STATISTICS:
			result = getStatistics(info);
			break;
		case MessageCode::JOIN_ROOM:
			result = joinRoom(info);
			break;
		case MessageCode::CREATE_ROOM:
			result = createRoom(info);
			break;
		case MessageCode::LOGOUT:
			result = logout(info);
			break;
	}
	return result;
}

void MenuRequestHandler::disconnect(){}

/*
disconnect user from server.
input: request information.
output: result.
*/
RequestResult MenuRequestHandler::logout(const RequestInfo& info)
{
	response::Logout response = {
		STATUS_SUCCESS
	};
	
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	if (!m_handlerFactory->getLoginManager().logout(m_user.getUsername())) //try to logout
	{
		response.status = STATUS_FAIL;
		result.newHandler = m_handlerFactory->createMenuRequestHandler(std::move(m_user));
	}
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

/*
get list of all available rooms.
input: request information.
output: result.
*/
RequestResult MenuRequestHandler::getRooms(const RequestInfo& info)
{
	response::GetRooms response = {
		std::vector<RoomData>()
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};
	
	response.rooms.reserve(m_handlerFactory->getRoomManager().getRooms().size());
	for (const auto& room : m_handlerFactory->getRoomManager().getRooms())
	{
		if (isRoomAvailable(room.second))
		{
			response.rooms.push_back(room.second.getRoomData()); //insert room metadata
		}
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createMenuRequestHandler(std::move(m_user));

	return result;
}

//checks if a given room is avaliable
bool MenuRequestHandler::isRoomAvailable(const Room& room)
{
	return (room.isPublic() || (room.doesUserExist(m_user) && room.getRank(m_user) == PlayerRank::INVITED))
			&& !room.isGameRunning() && room.getRoomData().curPlayers < room.getRoomData().maxPlayers;
}

/*
get statistics of the current logged user.
input: request information.
output: result.
*/
RequestResult MenuRequestHandler::getStatistics(const RequestInfo& info)
{
	response::GetStatistics response = {
		std::vector<std::string>()
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	response.statistics = m_handlerFactory->getStatisticsManager().getStatistics(m_user); //get statistics of current user
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createMenuRequestHandler(std::move(m_user));

	return result;
}

/*
join current logged user to spesific room.
input: request information.
output: result.
*/
RequestResult MenuRequestHandler::joinRoom(const RequestInfo& info)
{
	response::JoinRoom response = {
		STATUS_FAIL
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	request::JoinRoom request = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(info.buffer);

	if (m_handlerFactory->getRoomManager().doesRoomExist(request.id) && isRoomAvailable(m_handlerFactory->getRoomManager()[request.id]) && m_handlerFactory->getRoomManager()[request.id].add(m_user)) //add the user
	{
		response.status = STATUS_SUCCESS;
		result.newHandler = m_handlerFactory->createRoomRequestHandler(request.id, std::move(m_user)); //create room request handler
	}
	else
	{
		result.newHandler = m_handlerFactory->createMenuRequestHandler(std::move(m_user)); //create menu request handler
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

/*
create new room.
input: request information.
output: result.
*/
RequestResult MenuRequestHandler::createRoom(const RequestInfo& info)
{
	response::CreateRoom response = {
		STATUS_FAIL
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	request::CreateRoom request = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(info.buffer);

	//create room
	if (request.maxUsers > 0 && Protector::isSafe(request.roomName))
	{
		unsigned int roomid = m_handlerFactory->getRoomManager().create(m_user, request.roomName, request.isPublic, request.maxUsers);
		if (roomid)
		{
			response.status = STATUS_SUCCESS;
			result.newHandler = m_handlerFactory->createRoomRequestHandler(roomid, std::move(m_user)); //create room request handler
		}
	}

	if (response.status == STATUS_FAIL) //if failed to create room
	{
		result.newHandler = m_handlerFactory->createMenuRequestHandler(std::move(m_user)); //create menu request handler
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}
