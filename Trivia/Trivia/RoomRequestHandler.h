#pragma once
#include <string>
#include "RequestHandlerFactory.h"

using std::string;

class RequestHandlerFactory;

class RoomRequestHandler : public IRequestHandler
{
private:
	unsigned int m_roomid;
	LoggedUser m_user;
	RequestHandlerFactory* m_handlerFactory;

	RequestResult getRoomInfo(const RequestInfo& info);
	RequestResult leaveRoom(const RequestInfo& info);
	RequestResult promote(const RequestInfo& info);
	RequestResult demote(const RequestInfo& info);
	RequestResult invite(const RequestInfo& info);
	RequestResult uninvite(const RequestInfo& info);
	RequestResult getRank(const RequestInfo& info);
	RequestResult closeRoom(const RequestInfo& info);
	RequestResult startGame(const RequestInfo& info);
	RequestResult getQuestionCategories(const RequestInfo& info);
	RequestResult canPlay(const RequestInfo& info);
public:
	RoomRequestHandler(unsigned int roomid, const LoggedUser& user, RequestHandlerFactory* handlerFactory);
	RoomRequestHandler(unsigned int roomid, LoggedUser&& user, RequestHandlerFactory* handlerFactory);
	~RoomRequestHandler() = default;
	bool isRequestRelevant(const MessageCode code) override;
	RequestResult handleRequest(const RequestInfo& info) override;
	void disconnect() override;
};

