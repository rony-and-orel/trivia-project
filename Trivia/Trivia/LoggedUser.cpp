#include "LoggedUser.h"

//C'tor
LoggedUser::LoggedUser(const std::string& username) :
	m_username(username)
{}
LoggedUser::LoggedUser(std::string&& username) noexcept:
	m_username(std::move(username))
{}
LoggedUser::LoggedUser(LoggedUser&& other) noexcept :
	m_username(std::move(other.m_username))
{}

//== operator
bool LoggedUser::operator==(const LoggedUser& other) const
{
	return this->m_username == other.m_username;
}

//== operator for comparing with a username directly
bool LoggedUser::operator==(const std::string& other) const
{
	return this->m_username == other;
}

//returns the username
const std::string& LoggedUser::getUsername() const
{
	return this->m_username;
}