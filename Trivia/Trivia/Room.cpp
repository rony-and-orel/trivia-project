#include "Room.h"

//C'tor
Room::Room(const LoggedUser& creator, unsigned int id, const std::string& name, bool isPublic, unsigned short maxPlayers):
	m_metadata({ id, name, maxPlayers, 1, isPublic, false }), m_game(nullptr)
{
	m_users.emplace(creator, PlayerRank::CREATOR);
}

//D'tor
Room::~Room()
{
	if (m_game != nullptr)
	{
		delete m_game;
		m_game = nullptr;
	}
}

//returns the room's data struct
const RoomData& Room::getRoomData() const
{
	return m_metadata;
}

//check if a users exists in room
bool Room::doesUserExist(const LoggedUser& user) const
{
	return m_users.find(user) != m_users.end();
}

/*
adds a User to room. won't work when a game is running.
input: user to add.
output: if operation was successful.
*/
bool Room::add(const LoggedUser& user)
{
	std::unique_lock<std::mutex> lck(m_mutex);
	bool flag = m_metadata.curPlayers < m_metadata.maxPlayers;
	if (flag)
	{
		if (m_metadata.isPublic)
		{
			flag = m_users.emplace(user, PlayerRank::MEMBER).second;
		}
		else
		{
			auto it = m_users.find(user);
			if (it != m_users.end() && (*it).second > PlayerRank::MEMBER)
			{
				(*it).second = PlayerRank::MEMBER;
				flag = true;
			}
		}
	}
	if (flag)
	{
		m_metadata.curPlayers++;
	}
	return flag;
}

/*
invites a user to room.
input: user.
output: if successful.
*/
bool Room::invite(const LoggedUser& user)
{
	std::unique_lock<std::mutex> lck(m_mutex);
	bool flag = false;
	if (!m_metadata.isPublic)
	{
		flag = m_users.emplace(user, PlayerRank::INVITED).second;
	}
	return flag;
}

/*
uninvites a user to room.
input: user.
output: if successful.
*/
bool Room::uninvite(const LoggedUser& user)
{
	std::unique_lock<std::mutex> lck(m_mutex);
	bool flag = false;
	auto it = m_users.find(user);
	if ((*it).second == PlayerRank::INVITED)
	{
		m_users.erase(it);
		flag = true;
	}
	return flag;
}

/*
removes a user from room and game.
input: user to remove.
output: none.
*/
void Room::remove(const LoggedUser& user)
{
	auto findAdmin = [](const std::pair<const LoggedUser, PlayerRank>& user) -> bool
	{
		return user.second == PlayerRank::ADMIN;
	};

	auto findMember = [](const std::pair<const LoggedUser, PlayerRank>& user) -> bool
	{
		return user.second == PlayerRank::MEMBER;
	};

	std::unique_lock<std::mutex> lck(m_mutex);
	PlayerRank rank = getRank(user);
	if (m_users.erase(user) && (!m_metadata.isGameRunning) || m_game->m_players.erase(user))
	{
		if (rank == PlayerRank::CREATOR) //if creator has left, promote a player to creator
		{
			auto it = std::find_if(m_users.begin(), m_users.end(), findAdmin);

			if (it != m_users.end())
			{
				(*it).second = PlayerRank::CREATOR;
			}
			else
			{
				it = std::find_if(m_users.begin(), m_users.end(), findMember);

				if (it != m_users.end())
				{
					(*it).second = PlayerRank::CREATOR;
				}
			}
		}
		if (m_game != nullptr)
		{
			m_game->m_playerCloseCount--;
		}
		m_metadata.curPlayers--;
	}
}

//returns the rank of a player
PlayerRank Room::getRank(const LoggedUser& user) const
{
	return (*m_users.find(user)).second;
}

/*
promotes a user to admin.
input: user.
output: none.
*/
void Room::promote(const LoggedUser& user)
{
	auto it = m_users.find(user);
	if ((*it).second == PlayerRank::MEMBER)
	{
		(*it).second = PlayerRank::ADMIN;
	}
}

/*
demotes a user from admin.
input: user.
output: none.
*/
void Room::demote(const LoggedUser& user)
{
	auto it = m_users.find(user);
	if ((*it).second == PlayerRank::ADMIN)
	{
		(*it).second = PlayerRank::MEMBER;
	}
}

std::vector<PlayerResults> Room::getResultsFromLastGame() const
{
	return m_lastGameResults;
}

//returns if a game is running
bool Room::isGameRunning() const
{
	return m_metadata.isGameRunning;
}

//returns if the room is public
bool Room::isPublic() const
{
	return m_metadata.isPublic;
}

//returns a pointer to the game
Game* Room::operator->() const
{
	return m_game;
}

//returns refrence to the user map
const std::unordered_map<LoggedUser, PlayerRank>& Room::getUsers() const
{
	return m_users;
}