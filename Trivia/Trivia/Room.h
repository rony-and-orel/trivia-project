#pragma once
#include <string>
#include <unordered_set>
#include <mutex>
#include "Definitions.h"
#include "LoggedUser.h"
#include "Game.h"

class Room
{
private:
	RoomData m_metadata;
	std::unordered_map<LoggedUser, PlayerRank> m_users;
	Game* m_game;
	std::mutex m_mutex;
	std::vector<PlayerResults> m_lastGameResults;
public:
	explicit Room(const LoggedUser& creator, unsigned int id, const std::string& name, bool isPublic, unsigned short maxPlayers);
	Room(const Room& other) = delete;
	~Room();

	const RoomData& getRoomData() const;
	bool doesUserExist(const LoggedUser& user) const;
	bool add(const LoggedUser& user);
	bool invite(const LoggedUser& user);
	bool uninvite(const LoggedUser& user);
	void remove(const LoggedUser& user);
	PlayerRank getRank(const LoggedUser& user) const;
	void promote(const LoggedUser& user);
	void demote(const LoggedUser& user);
	
	std::vector<PlayerResults> getResultsFromLastGame() const;
	
	bool isGameRunning() const;
	bool isPublic() const;
	Game* operator ->() const;
	const std::unordered_map<LoggedUser, PlayerRank>& getUsers() const;

	friend class RoomManager;
};

