#include "Server.h"

//C'tor
Server::Server() :
	m_communicator()
{
	
}

//D'tor
Server::~Server()
{
	
}

/*
function will execute the server.
input: none.
output: none.
*/
void Server::run()
{
	string inputFromConsole = ""; //user input

	thread(&Communicator::startHandleRequests, &m_communicator).detach(); //open communicator thread

	paint("notify");
	std::cout << "Trivia server is now running on port @" << SERVER_PORT << std::endl;

	try
	{
		while (true)
		{
			paint("standard");
			std::cout << ">> ";
			std::cin >> inputFromConsole; //get imput from user

			if (_console_commands.find(inputFromConsole) != _console_commands.end()) //check if command exists
			{
				Server::console_function_handler handler = _console_commands.at(inputFromConsole); //get function by command
				(this->*handler)(); //run command
			}
			else
			{
				//print messages
				paint("error");
				std::cout << "Error: no such command as '" << inputFromConsole << "'." << std::endl;
				paint("notify");
				std::cout << "Notify: you can use the 'help' command to get the list of supported commands.\n" << std::endl;
				paint("standard");
			}
		}
	}
	catch(signals::ShutdownSignal& shutdownSignal)
	{
		//Do nothing...
	}
}

/*
paints the console output text by code name.
input: paint code name.
output: none.s
*/
void Server::paint(const std::string& paint)
{
	//paint code map
	const map<std::string, WORD> paints = {
		{ "warning"	,	0x06},
		{ "error"	,	0x04},
		{ "notify"	,	0x03},
		{ "ok"		,	0x0a},
		{ "standard",	0x07}
	};
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	if (paints.find(paint) != paints.end())
	{
		SetConsoleTextAttribute(hConsole, (*paints.find(paint)).second); //change color
	}
}

/*
send shutdown signal to the server.
input: none.
output: none.
*/
void Server::shutdown()
{
	string inputFromConsole = ""; //user input
	if (m_communicator.hasConnections()) //check if the communicator handles connections
	{
		//print messages
		paint("error");
		std::cout << "Error: cannot shutdown the server because there are still conversations handled in the background." << std::endl;
	}
	else
	{
		paint("ok");
		std::cout << "shuting down the server..." << std::endl;
		paint("standard");
		throw signals::ShutdownSignal(); //send shut down signal
	}
}

/*
clear screen operation.
input: none.
output: none.
*/
void Server::cls()
{
	system("cls");
}

/*
prints the help page of the server.
input: none.
output: none.
*/
void Server::help()
{
	paint("notify");
	std::cout << "\nUsage:\n--\t>> <type_any_command>\n\n"
				 "list of all supported commands:\n\n"
				 "* shutdown	- shuts down the server.\n"
				 "* cls		- clears the screen.\n"
				 "* help		- get list of supported commands.\n";
	std::cout << "\n";
}

void Server::moo()
{
	paint("ok");
	std::cout << 
		"         (__) \n"
		"         (oo)\n"
		"  / ------\\/\n"
		" / |     ||\n"
		"* / \\---/ \\\n"
		"  ~~     ~~\n"
		"....\"Have you mooed today?\"...\n" << std::endl;
	paint("standard");
}

//command map
const std::map<std::string, Server::console_function_handler> Server::_console_commands
{
	{ "shutdown", &Server::shutdown },
	{ "cls", &Server::cls },
	{ "help", &Server::help },
	{ "moo", &Server::moo} 
};