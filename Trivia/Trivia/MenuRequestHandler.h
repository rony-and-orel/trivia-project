#pragma once
#include <string>
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"

using std::string;

class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
private:
	LoggedUser m_user;
	RequestHandlerFactory* m_handlerFactory;

	RequestResult logout(const RequestInfo& info);
	RequestResult getRooms(const RequestInfo& info);
	RequestResult getStatistics(const RequestInfo& info);
	RequestResult joinRoom(const RequestInfo& info);
	RequestResult createRoom(const RequestInfo& info);

	bool isRoomAvailable(const Room& room);
public:
	MenuRequestHandler(RequestHandlerFactory* handlerFactory, const LoggedUser& user);
	MenuRequestHandler(RequestHandlerFactory* handlerFactory, LoggedUser&& user);
	~MenuRequestHandler() = default;
	bool isRequestRelevant(const MessageCode code) override;
	RequestResult handleRequest(const RequestInfo& info) override;
	void disconnect() override;
};

