#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include "Question.h"

#define SERVER_PORT 8826
#define HEADER_SIZE 5
#define STAT_COUNT 4
#define MIN_TIME_PER_QUESTION 5

#define DB_NAME "Trivia.db"

#define TWO 2
#define THREE 3

typedef unsigned char byte;

enum class MessageCode : byte
{
	//error
	ERROR = 0,

	//login
	SIGNUP = 10,
	LOGIN = 11,
	LOGOUT = 12,

	//players
	GET_STATISTICS = 21, //of player who requested

	//rooms
	GET_ROOMS = 30,
	GET_ROOM_INFO = 31, //... of the room player is currently in

	CREATE_ROOM = 33,
	JOIN_ROOM = 34,
	LEAVE_ROOM = 35,
	CLOSE_ROOM = 36, //... that the player is currently in

	PROMOTE = 37,
	DEMOTE = 38,
	INVITE = 39,
	UNINVITE = 40,
	GET_RANK = 41,
	START_GAME = 42, //start running game
	GET_QUESTION_CATEGORIES = 43,
	CAN_PLAY = 44,

	//game
	GET_QUESTION = 50,
	SUBMIT_ANSWER = 51,
	GET_GAME_RESULTS = 52,
	LEAVE_GAME = 53,
	GET_GAME_INFO = 54,
};

enum StatusCode : byte
{
	STATUS_SUCCESS = 1,
	STATUS_FAIL = 0
};

enum class PlayerRank : byte
{
	CREATOR = 0,
	ADMIN = 1,
	MEMBER = 2,
	INVITED = 3,
};

struct GameStatistics
{
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int totalAnswerTime;
	bool answeredQuestion;
};

struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned short maxPlayers;
	unsigned short curPlayers;
	bool isPublic;
	bool isGameRunning;
};

struct GameData
{
	unsigned short timeout;
	unsigned int questionCount;
	unsigned short currQuestionid;
};

struct PlayerResults
{
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int totalAnswerTime;
};

namespace signals
{
	class ShutdownSignal : public std::exception
	{

	};
};

namespace request
{
	struct Login
	{
		std::string username;
		std::string password;
	};

	struct Signup
	{
		std::string username;
		std::string password;
		std::string email;
		std::string address;
		std::string phoneNumber;
		std::string birthDate;
	};

	struct CreateRoom
	{
		std::string roomName;
		unsigned short maxUsers;
		bool isPublic;
	};

	struct JoinRoom
	{
		unsigned int id;
	};

	struct Promote
	{
		std::string user;
	};

	struct Demote
	{
		std::string user;
	};

	struct Invite
	{
		std::string user;
	};

	struct Uninvite
	{
		std::string user;
	};

	struct SubmitAnswer
	{
		unsigned short answerid;
	};

	struct StartGame
	{
		unsigned short timePerQuestion;
		unsigned short roundCount;
		byte diff;
		std::string category;
	};
}

namespace response
{
	struct Error
	{
		std::string message;
	};

	struct Signup
	{
		StatusCode status;
	};

	struct Login
	{
		StatusCode status;
	};

	struct Logout
	{
		StatusCode status;
	};

	struct GetStatistics //of user
	{
		std::vector<std::string> statistics;
	};

	struct GetRooms
	{
		std::vector<RoomData> rooms;
	};

	struct GetRoomInfo
	{
		StatusCode status;
		RoomData room;
		std::vector<std::pair<std::string, PlayerRank>> players;
	};

	struct CreateRoom
	{
		StatusCode status;
	};

	struct JoinRoom
	{
		StatusCode status;
	};

	struct LeaveRoom
	{
		StatusCode status;
	};

	struct CloseRoom
	{
		StatusCode status;
	};

	struct Promote
	{
		StatusCode status;
	};

	struct Demote
	{
		StatusCode status;
	};

	struct Invite
	{
		StatusCode status;
	};

	struct Uninvite
	{
		StatusCode status;
	};

	struct GetRank
	{
		StatusCode status;
		PlayerRank rank;
	};

	struct StartGame
	{
		StatusCode status;
	};

	struct GetQuestionCategories
	{
		std::vector<std::string> categories;
	};

	struct getQuestion
	{
		StatusCode status;
		std::string question;
		unsigned short id;
		unsigned short time;
		std::array<std::string, FOUR> answers;
	};

	struct SubmitAnswer
	{
		StatusCode status;
		unsigned int correctAnswerId;
	};

	struct GetGameResults
	{
		StatusCode status;
		std::vector<PlayerResults> results;
	};

	struct LeaveGame
	{
		StatusCode status;
	};

	struct CanPlay
	{
		StatusCode status;
	};

	struct GetGameInfo
	{
		GameData info;
	};
}