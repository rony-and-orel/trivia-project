#pragma once
#include "Definitions.h"

class JsonResponsePacketSerializer
{
private:
	static std::vector<byte> generateResponse(const std::string&& jsonStr, MessageCode code);
	static std::string parseVector(const std::vector<std::string>& args);
public:
	static std::vector<byte> serializeResponse(response::Error& res);
	static std::vector<byte> serializeResponse(response::Signup& res);
	static std::vector<byte> serializeResponse(response::Login& res);
	static std::vector<byte> serializeResponse(response::Logout& res);
	static std::vector<byte> serializeResponse(response::GetStatistics& res);
	static std::vector<byte> serializeResponse(response::GetRooms& res);
	static std::vector<byte> serializeResponse(response::GetRoomInfo& res);
	static std::vector<byte> serializeResponse(response::CreateRoom& res);
	static std::vector<byte> serializeResponse(response::JoinRoom& res);
	static std::vector<byte> serializeResponse(response::LeaveRoom& res);
	static std::vector<byte> serializeResponse(response::CloseRoom& res);
	static std::vector<byte> serializeResponse(response::Promote& res);
	static std::vector<byte> serializeResponse(response::Demote& res);
	static std::vector<byte> serializeResponse(response::Invite& res);
	static std::vector<byte> serializeResponse(response::Uninvite& res);
	static std::vector<byte> serializeResponse(response::GetRank& res);
	static std::vector<byte> serializeResponse(response::StartGame& res);
	static std::vector<byte> serializeResponse(response::GetQuestionCategories& res);
	static std::vector<byte> serializeResponse(response::getQuestion& res);
	static std::vector<byte> serializeResponse(response::SubmitAnswer& res);
	static std::vector<byte> serializeResponse(response::GetGameResults& res);
	static std::vector<byte> serializeResponse(response::LeaveGame& res);
	static std::vector<byte> serializeResponse(response::CanPlay& res);
	static std::vector<byte> serializeResponse(response::GetGameInfo& res);
};

