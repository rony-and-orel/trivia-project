#pragma once
#include <string>
#include <functional>

class LoggedUser
{
private:
	std::string m_username;
public:
	explicit LoggedUser(const std::string& username);
	explicit LoggedUser(std::string&& username) noexcept;
	explicit LoggedUser(const LoggedUser&) = default;
	explicit LoggedUser(LoggedUser&& other) noexcept;
	~LoggedUser() = default;
	bool operator==(const LoggedUser& other) const;
	bool operator==(const std::string& other) const;
	const std::string& getUsername() const;
	friend std::hash<LoggedUser>;
};

namespace std
{
	//std::hash implementation for LoggedUser
	template <>
	struct hash<LoggedUser>
	{
		std::size_t operator()(const LoggedUser& user) const
		{
			return ((std::hash<std::string>()(user.m_username)));
		}
	};
}