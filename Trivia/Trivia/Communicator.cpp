#include "Communicator.h"

//C'tor
Communicator::Communicator()
{
	WSADATA wsa_data = { };
	if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
	{
		throw std::exception("WSAStartup Failed");
	}	

	//establishe TCP socket
	_listeningSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_listeningSocket == INVALID_SOCKET)
	{
		throw std::exception("Error in function " __FUNCTION__ " : listening socket creation was invalid");
	}
		
}

//D'tor
Communicator::~Communicator()
{
	try
	{
		closesocket(_listeningSocket);
		try
		{
			WSACleanup();
		}
		catch (...) {}
	}
	catch (...) {}
}

/*
handles a new client connection
input: client socket
output: none
*/
void Communicator::handleNewClient(SOCKET clientSocket)
{
	map<SOCKET, std::unique_ptr<IRequestHandler>>::iterator client = m_clients.find(clientSocket);

	RequestInfo request; // request information
	RequestResult response; // request results

	if (client != m_clients.end())
	{
		try
		{
			while (true)
			{
				response = { vector<byte>() , nullptr };
				request = receiveRequest(clientSocket);

				if ((*client).second != nullptr && !request.buffer.empty() && (*client).second->isRequestRelevant((MessageCode)request.buffer[0])) //check if relevant
				{
					response = (*client).second->handleRequest(request); //handle request
				}
				else
				{
					sendError(clientSocket, "invalid request", response);
					throw exception("invalid request");
				}
				sendResponse(clientSocket, response);
			}
		}
		catch (const exception&)
		{
			if (client->second.get() != nullptr)
			{
				client->second.get()->disconnect();	//disconnect user from the room or the game
			}
			closesocket(clientSocket); //close connection

			lock_guard<mutex> lck(clientsLock);
			m_clients.erase(client); //remove from list
		}
	}
}

/*
receives request from client.
input: refrence to response, client socket.
output: none.
*/
RequestInfo Communicator::receiveRequest(SOCKET clientSocket)
{
	uint32_t length = 0; //data length
	short i = 0;

	RequestInfo request{ vector<byte>(HEADER_SIZE) };

	int recvExitCode = recv(clientSocket, (char*)request.buffer.data(), HEADER_SIZE, NULL); //receive header

	if (recvExitCode <= 0) //check if socket was closed
	{
		throw exception("connection was closed");
	}

	//get length of data
	for (i = 0; i < sizeof(uint32_t); i++)
	{
		((byte*)&length)[i] = request.buffer[i + 1];
	}
	request.buffer.resize(HEADER_SIZE + length);

	if (length != 0)
	{
		recv(clientSocket, (char*)(request.buffer.data() + HEADER_SIZE), length, NULL); //receive data
	}
	return request;
}

/*
sends response to client.
input: refrence to response, client socket.
output: none.
*/
void Communicator::sendResponse(SOCKET clientSocket, const RequestResult& response)
{
	int sendExitCode = 0;

	unique_lock<mutex> lck(sendLock);
	sendExitCode = send(clientSocket, (char*)response.response.data(), response.response.size(), NULL); //send response
	lck.unlock();

	m_clients.find(clientSocket)->second = std::unique_ptr<IRequestHandler>(response.newHandler); //set new handle

	if (sendExitCode == SOCKET_ERROR) //check if socket was closed
	{
		throw exception("connection was closed");
	}
}

/*
sends error message to client.
input: refrence to response, client socket, error message.
output: none.
*/
void Communicator::sendError(SOCKET clientSocket, const string& errorMessage, RequestResult& response)
{
	//error response
	response::Error error = {
		errorMessage
	};

	response.response = JsonResponsePacketSerializer::serializeResponse(error); //get valid response
	send(clientSocket, (char*)response.response.data(), response.response.size(), NULL); //send error message
}

/*
checks if the communicator handles connections.
input: none.
output: statement.
*/
bool Communicator::hasConnections()
{
	return !m_clients.empty();
}

/*
starts the communicator operation.
input: none.
output: none.
*/
void Communicator::startHandleRequests()
{
	struct sockaddr_in sa = { 0 };

	// setup socket address
	sa.sin_port = htons(SERVER_PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;


	// connect socket to port by configuration
	if (bind(_listeningSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception("Error in function " __FUNCTION__ " : faild to bind socket");
	}

	// start listening
	if (listen(_listeningSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception("Error in function " __FUNCTION__ " : faild to listen to socket");
	}

	while (true)
	{
		SOCKET client_socket = ::accept(_listeningSocket, NULL, NULL); //accept

		if (client_socket == INVALID_SOCKET)
		{
			throw std::exception("Error in function " __FUNCTION__ " : failed to establish the client connection");
		}

		unique_lock<mutex> lck(clientsLock);
		m_clients.emplace(client_socket, m_handlerFactory.createLoginRequestHandler()); //insert to map
		lck.unlock();

		// create new thread for client
		thread(&Communicator::handleNewClient, this, client_socket).detach();
	}
}
