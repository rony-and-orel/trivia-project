#pragma once
#include "IDatabase.h"

#include <string>
#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/types.hpp>
#include <mongocxx/exception/exception.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/uri.hpp>
#include <bsoncxx/document/value.hpp>
#include <bsoncxx/document/view.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>

#pragma warning(disable : 4996)

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;
using std::string;

class MongoDatabase : public IDatabase
{
private:
	static mongocxx::instance _instance; //mongoDB instance
	static mongocxx::client _connection; //client connection to database

	mongocxx::database _db; //mongoDB database

	int getNewId(string collection);
	bool existsInCollection(int triviaId, string collection);
public:
	explicit MongoDatabase();
	MongoDatabase(const MongoDatabase&) = delete;
	~MongoDatabase();

	bool doesUserExist(const std::string& username) const override;
	bool doesPasswordMatch(const std::string& username, const std::string& password) const override;
	void addNewUser(const std::string& username, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birthDate) override;

	std::vector<std::string> getCategories() const override;
	std::queue<Question> getQuestions(unsigned short count, byte diff, const std::string& category = "") const override;

	float getPlayerAverageAnswerTime(const std::string& username) const override;
	void incrementPlayerTotalAnswerTime(const std::string& username, unsigned int answerTime) override;

	unsigned int getNumOfCorrectAnswers(const std::string& username) const override;
	void incrementNumOfCorrectAnswers(const std::string& username, unsigned int correctAnswerCount) override;

	unsigned int getNumOfWrongAnswers(const std::string& username) const override;
	void incrementNumOfWrongAnswers(const std::string& username, unsigned int wrongAnswerCount) override;

	unsigned int getNumOfPlayerGames(const std::string& username) const override;
	void incrementNumOfPlayerGames(const std::string& username) override;
};


