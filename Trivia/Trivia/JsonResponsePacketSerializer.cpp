#include "JsonResponsePacketSerializer.h"

/*
Serializes a generic protocol packet
input: json string to parse, and the packet code.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::generateResponse(const std::string&& jsonStr, MessageCode code)
{
	short i = 0;
	uint32_t jsonLength = jsonStr.length();
	std::vector<byte> buffer;
	buffer.reserve(HEADER_SIZE + jsonLength);

	buffer.push_back((byte)code);
	for (i = 0; i < sizeof(uint32_t); i++)
	{
		buffer.push_back(((byte*)&jsonLength)[i]);
	}
	buffer.insert(buffer.begin() + HEADER_SIZE, jsonStr.begin(), jsonStr.end());
	return buffer;
}

/*
Parses a vector is string, to a json compatible from.
input: vector of the strings.
output: string parsed to vector.
*/
std::string JsonResponsePacketSerializer::parseVector(const std::vector<std::string>& args)
{
	std::string str("[");
	for (const std::string& arg : args)
	{
		str += '\"' + arg + "\",";
	}
	if (args.size() > 0)
	{
		str.pop_back();
	}
	return str + "]";
}

/*
Serializes an Error packet.
	json fields: "message" - string.
input: ErrorResponse packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::Error& res)
{
	return generateResponse("{\"message\": \"" + res.message + "\"}", MessageCode::ERROR);
}

/*
Serializes a Signup packet.
	json fields: "status" - 1 or 0.
input: SignupResponse packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::Signup& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::SIGNUP);
}

/*
Serializes a Login packet.
	json fields: "status" - 1 or 0.
input: LoginResponse packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::Login& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::LOGIN);
}

/*
Serializes a Logout packet.
	json fields: "status" - 1 or 0.
input: Logout packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::Logout& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::LOGOUT);
}

/*
Serializes a GetStatistics packet.
	json fields: "games_played" - unsigned int; "correct_answers" - unsigned int; "wrong_answers" - unsigned int; "avg_time_per_answer" - float.
input: GetStatistics packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::GetStatistics& res)
{
	return generateResponse("{\"games_played\":" + res.statistics[0] + ", \"correct_answers\":" + res.statistics[1] + ", \"wrong_answers\":" + res.statistics[TWO] + ", \"avg_time_per_answer\":" + res.statistics[THREE] + "}", MessageCode::GET_STATISTICS);
}

/*
Serializes a GetRooms packet.
	json fields: "rooms" - [{"id" - unsigned int; "name" - string; "max_players" - unsigned short; "cur_players" - unsigned short; "is_public" - 1 or 0; "is_game_running" - 1 or 0}, ...]
input: GetRooms packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::GetRooms& res)
{
	std::string msg("{\"rooms\": [");

	for (const auto& room : res.rooms)
	{
		msg += "{\"id\": " + std::to_string(room.id) + ", \"name\": \"" + room.name + "\", \"max_players\": " + std::to_string(room.maxPlayers) + ", \"cur_players\": " + std::to_string(room.curPlayers) + ", \"is_public\": " + std::to_string(room.isPublic) + ", \"is_game_running\": " + std::to_string(room.isGameRunning) + "},";
	}
	if (res.rooms.size() > 0)
	{
		msg.pop_back();
	}
	return generateResponse(msg + "]}", MessageCode::GET_ROOMS);
}

/*
Serializes a GetRooms packet.
	json fields: "status" - 1 or 0; "room" - {"id" - unsigned int; "name" - string; "max_players" - unsigned short; "cur_players" - unsigned short; "is_public" - 1 or 0; "is_game_running" - 1 or 0}, "players" - [{"username" - string; "state" - PlayerRank enum}, {...}, ...].
input: GetRooms packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::GetRoomInfo& res)
{
	auto parsePlayers = [&]() -> std::string
	{
		std::string str("[");
		for (auto& player : res.players)
		{
			str += "{\"username\":\"" + player.first + "\", \"state\": " + std::to_string((byte)player.second) + "},";
		}
		if (res.players.size() > 0)
		{
			str.pop_back();
		}
		return str + ']';
	};
	return generateResponse("{\"status\": " + std::to_string(res.status) + ", \"room\": { \"id\": " + std::to_string(res.room.id) + ", \"name\": \"" + res.room.name + "\", \"max_players\": " + std::to_string(res.room.maxPlayers) + ", \"cur_players\": " + std::to_string(res.room.curPlayers) + ", \"is_public\": " + std::to_string(res.room.isPublic) + ", \"is_game_running\": " + std::to_string(res.room.isGameRunning) + "}, \"players\": " + parsePlayers() + "}", MessageCode::GET_ROOM_INFO);
}

/*
Serializes a CreateRoom packet.
	json fields: "status" - 1 or 0.
input: CreateRoom packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::CreateRoom& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::CREATE_ROOM);
}

/*
Serializes a JoinRoom packet.
	json fields: "status" - 1 or 0.
input: JoinRoom packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::JoinRoom& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::JOIN_ROOM);
}

/*
Serializes a LeaveRoom packet.
	json fields: "status" - 1 or 0;
input: LeaveRoom packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::LeaveRoom& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::LEAVE_ROOM);
}

/*
Serializes a CloseRoom packet.
	json fields: "status" - 1 or 0;
input: CloseRoom packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::CloseRoom& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::CLOSE_ROOM);
}

/*
Serializes a Promote packet.
	json fields: "status" - 1 or 0.
input: Promote packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::Promote& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::PROMOTE);
}
/*
Serializes a Demote packet.
	json fields: "status" - 1 or 0.
input: Demote packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::Demote& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::DEMOTE);
}

/*
Serializes an Invite packet.
	json fields: "status" - 1 or 0.
input: Invite packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::Invite& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::INVITE);
}

/*
Serializes an Uninvite packet.
	json fields: "status" - 1 or 0.
input: Uninvite packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::Uninvite& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::UNINVITE);
}

/*
Serializes an IsAdmin packet.
	json fields: "status" - 1 or 0.
input: IsAdmin packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::GetRank& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + ", \"rank\": " + std::to_string((byte)res.rank) + "}", MessageCode::GET_RANK);
}

/*
Serializes a StartGame packet.
	json fields: "status" - 1 or 0;
input: StartGame packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::StartGame& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::START_GAME);
}

/*
Serializes a GetQuestionCategories packet.
	json fields: "categories" - [category - string, categoey, ...];
input: GetQuestionCategories packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::GetQuestionCategories& res)
{
	return generateResponse("{\"categories\": " + parseVector(res.categories) + "}", MessageCode::GET_QUESTION_CATEGORIES);
}

/*
Serializes a getQuestion packet.
	json fields: "status" - 1 or 0; "question" - string; "id" - int; "time" - int; "answers" - [answer0 - string, answer1, answer2, answer3].
input: getQuestion packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::getQuestion& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + ", \"question\": \"" + res.question + "\", \"id\": " + std::to_string(res.id) + ", \"time\": " + std::to_string(res.time) + ", \"answers\": [\"" + res.answers[0] + "\", \"" + res.answers[1] + "\", \"" + res.answers[TWO] + "\", \"" + res.answers[THREE] + "\"]}", MessageCode::GET_QUESTION);
}

/*
Serializes a SubmitAnswer packet.
	json fields: "status" - 1 or 0; "correct_answer_id" - unsigned int.
input: SubmitAnswer packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::SubmitAnswer& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + ", \"correct_answer_id\": " + std::to_string(res.correctAnswerId) + "}", MessageCode::SUBMIT_ANSWER);
}

/*
Serializes a GetGameResults packet.
	json fields: "status" - 1 or 0; "results" - [{"player" - string; "correct_answer_count" - int; "wrong_answer_count" - int; "average_answer_time" - int}, ...].
input: GetGameResults packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::GetGameResults& res)
{
	auto parseResults = [&]() -> std::string
	{
		std::string str("[");
		for (auto& result : res.results)
		{
			str += "{\"username\":\"" + result.username + "\", \"correct_answer_count\":" + std::to_string(result.correctAnswerCount) + ", \"wrong_answer_count\":" + std::to_string(result.wrongAnswerCount) + ", \"total_answer_time\":" + std::to_string(result.totalAnswerTime) + "},";
		}
		if (res.results.size() > 0)
		{
			str.pop_back();
		}
		return str + ']';
	};
	return generateResponse("{\"status\": " + std::to_string(res.status) + ", \"results\": " + parseResults() + "}", MessageCode::GET_GAME_RESULTS);
}

/*
Serializes a LeaveGame packet.
	json fields: "status" - 1 or 0.
input: LeaveGame packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::LeaveGame& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::LEAVE_GAME);
}

/*
Serializes a CanPlay packet.
	json fields: "status" - 1 or 0.
input: CanPlay packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::CanPlay& res)
{
	return generateResponse("{\"status\": " + std::to_string(res.status) + "}", MessageCode::CAN_PLAY);
}

/*
Serializes a LeaveGame packet.
	json fields: "question_amount" - the amount of questions, "timeout" - time per question.
input: LeaveGame packet struct.
output: byte array.
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(response::GetGameInfo& res)
{
	return generateResponse("{\"question_amount\": " + std::to_string(res.info.questionCount) + ", \"timeout\":" + std::to_string(res.info.timeout) + "}", MessageCode::GET_GAME_INFO);
}
