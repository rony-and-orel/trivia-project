#include "RequestHandlerFactory.h"

/*
C'tor.
input: database manager.
output: none.
*/
RequestHandlerFactory::RequestHandlerFactory() :
	m_database(new SqliteDatabase(DB_NAME)), m_loginManager(m_database), m_roomManager(m_database), m_statisticsManager(m_database)
{}

/*
D'tor.
input: none.
output: none.
*/
RequestHandlerFactory::~RequestHandlerFactory()
{
	delete m_database;
	m_database = nullptr;
}

/*
login request handler creator.
input: none.
output: login request handler.
*/
LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(this);
}

/*
menu request handler creator.
input: none.
output: menu request handler.
*/
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(const LoggedUser& user)
{
	return new MenuRequestHandler(this, user);
}
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser&& user)
{
	return new MenuRequestHandler(this, std::move(user));
}

/*
room request handler creator.
input: none.
output: room request handler.
*/
RoomRequestHandler* RequestHandlerFactory::createRoomRequestHandler(unsigned int roomid, const LoggedUser& user)
{
	return new RoomRequestHandler(roomid, user, this);
}
RoomRequestHandler* RequestHandlerFactory::createRoomRequestHandler(unsigned int roomid, LoggedUser&& user)
{
	return new RoomRequestHandler(roomid, std::move(user), this);
}

/*
game request handler creator.
input: none.
output: game request handler.
*/
GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(Room& room, const LoggedUser& user, unsigned int currQuestion)
{
	return new GameRequestHandler(room, user, currQuestion, this);
}
GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(Room& room, LoggedUser&& user, unsigned int currQuestion)
{
	return new GameRequestHandler(room, std::move(user), currQuestion, this);
}

/*
getter for the login manager.
input: none.
output: login manager.
*/
LoginManager& RequestHandlerFactory::getLoginManager()
{
	return m_loginManager;
}

/*
getter for the room manager.
input: none.
output: room manager.
*/
RoomManager& RequestHandlerFactory::getRoomManager()
{
	return m_roomManager;
}

/*
getter for the statistics manager.
input: none.
output: statistics manager.
*/
StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return m_statisticsManager;
}