#pragma once

#pragma comment (lib, "ws2_32.lib")
#include <string>
#include <exception>
#include <thread>
#include <map>
#include <mutex>
#include "MongoDatabase.h" //DON'T EVEN DARE TO TOUCH IT
#include <WinSock2.h>
#include <Windows.h>
#include "Definitions.h"
#include "RequestHandlerFactory.h"

using std::mutex;
using std::map;
using std::exception;
using std::thread;
using std::string;
using std::lock_guard;
using std::unique_lock;

static mutex clientsLock;
static mutex sendLock;

class Communicator
{
private:
	SOCKET _listeningSocket;
	map<SOCKET, std::unique_ptr<IRequestHandler>> m_clients;
	RequestHandlerFactory m_handlerFactory;

	void handleNewClient(SOCKET clientSocket);
	RequestInfo receiveRequest(SOCKET clientSocket);
	void sendResponse(SOCKET clientSocket, const RequestResult& response);
	void sendError(SOCKET clientSocket, const string& errorMessage, RequestResult& response);
public:
	Communicator();
	Communicator(const Communicator&) = delete;
	~Communicator();
	void startHandleRequests();
	bool hasConnections();
};

