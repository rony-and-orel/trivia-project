#include "MongoDatabase.h"

//mongoDB connection instance - should kept alive constantly
mongocxx::instance MongoDatabase::_instance{};
mongocxx::client MongoDatabase::_connection{ mongocxx::uri{} }; //connecting to server (local host)

/*
get new trivia id from a collection.
input: none.
output: new id.
*/
int MongoDatabase::getNewId(string collection)
{
	int newId = 0;
	mongocxx::collection coll = this->_db[collection]; //get collection

	//set up sorting order 
	auto order = bsoncxx::builder::stream::document{} << "$natural" << 1 << bsoncxx::builder::stream::finalize;
	auto sorter = mongocxx::options::find{};
	sorter.sort(order.view());

	auto cursor = coll.find({}, sorter); //find and sort
	if (existsInCollection(1, collection)) //if collection is not empty
	{
		//get the last gallery id from the collection
		for (auto&& document : cursor)
		{
			newId = document["trivia_id"].get_int32().value;
		}
	}

	return newId + 1;
}

/*
checks if the trivia id exists in the collection.
input: gallery id, collection name.
output: statement.
*/
bool MongoDatabase::existsInCollection(int triviaId, string collection)
{
	mongocxx::collection coll = this->_db[collection];

	auto cursor = coll.find_one(make_document(kvp("trivia_id", triviaId))); //find
	return cursor.has_value();
}

//C'tor
MongoDatabase::MongoDatabase()
{
	this->_db = this->_connection["Trivia"]; //connect to database
}

//D'tor
MongoDatabase::~MongoDatabase()
{
}

/*
check if user exists.
input: user name.
output: if user exists flag.
*/
bool MongoDatabase::doesUserExist(const std::string& username) const
{
	mongocxx::collection albums = this->_db["users"];

	auto cursor = albums.find_one(make_document(kvp("username", username))); //find

	return cursor.has_value();
}

/*
compares the given password with the one stored in the database.
input: user name, password.
outpu: true/false statement.
*/
bool MongoDatabase::doesPasswordMatch(const std::string& username, const std::string& password) const
{
	mongocxx::collection users = this->_db["users"];

	auto cursor = users.find_one(make_document(kvp("username", username)));
	bsoncxx::document::view user = cursor.get().view();

	return Protector::compareHash(password, user["salt"].get_utf8().value.to_string(), Protector::toBinary(user["hash"].get_utf8().value.to_string()));
}

/*
adds new user to database.
input: user name, password, email, address, phone, birth date.
output: none.
*/
void MongoDatabase::addNewUser(const std::string& username, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birthDate)
{
	Protector::Hash hash = Protector::generateNewHash(password);
	mongocxx::collection albums = this->_db["users"];
	//insert new user
	auto result = albums.insert_one(
		make_document(
			kvp("trivia_id", getNewId("users")),
			kvp("username", username),
			kvp("salt", hash.salt),
			kvp("hash", Protector::toStr(hash.digest)),
			kvp("email", email),
			kvp("address", address),
			kvp("phone", phone),
			kvp("birth_date", birthDate),
			kvp("statistics", make_document(
				kvp("total_answer_time", 0),
				kvp("correct_answer_count", 0),
				kvp("wrong_answer_count", 0),
				kvp("game_count", 0)))
		));
}

/*
gets all possible categories for questions.
input: none.
output: vector of all of the categories.
*/
std::vector<std::string> MongoDatabase::getCategories() const
{
	mongocxx::collection questions = this->_db["questions"];
	std::vector<std::string> categories = std::vector<std::string>();

	auto cursor = questions.distinct("category", bsoncxx::builder::stream::document().view()); //select distinct categories

	for (auto doc : cursor)
	{
		for (auto value : doc["values"].get_array().value)
		{
			categories.push_back(value.get_utf8().value.to_string());
		}
	}

	return categories;
}

/*
gets questions from the database.
input: how many questions are needed, difficulty, optional question category.
output: queue of the amount of questions requested, or 0 if none matching found.
*/
std::queue<Question> MongoDatabase::getQuestions(unsigned short count, byte diff, const std::string& category) const
{
	std::queue<Question> questions = std::queue<Question>();
	mongocxx::collection questions_coll = this->_db["questions"];

	mongocxx::pipeline pip; //aggregation pipeline

	if (category != "" && category !=  "Any" && diff <= TWO) //check if there is any category or difficulty
	{
		pip.match(make_document(kvp("category", category), kvp("difficulty", (int)diff)));
	}
	else if (diff <= TWO) //check if there is difficulty
	{
		pip.match(make_document(kvp("difficulty", (int)diff)));
	}
	else
	{
		pip.match(make_document()); //no filters
	}
	pip.sample(count); //sample number of questions
	
	for (auto&& question : questions_coll.aggregate(pip)) //iterate through the questions
	{
		//emplace in queue
		questions.emplace(question["question"].get_utf8().value.to_string(),
			std::array<std::string, THREE> {
			question["incorrect_answers"][0].get_utf8().value.to_string(),
				question["incorrect_answers"][1].get_utf8().value.to_string(),
				question["incorrect_answers"][TWO].get_utf8().value.to_string()},
			question["correct_answer"].get_utf8().value.to_string());
	}

	return questions;
}

/*
get the player's average answer time per game.
input: player username.
output: average answer time.
*/
float MongoDatabase::getPlayerAverageAnswerTime(const std::string& username) const
{
	float average_answer_time = 0;
	int total_answer_count = 0;
	mongocxx::collection users = this->_db["users"];

	auto cursor = users.find_one(make_document(kvp("username", username))); //find user document
	bsoncxx::document::view user = cursor.get().view(); //view document

	total_answer_count = user["statistics"]["wrong_answer_count"].get_int32().value + user["statistics"]["correct_answer_count"].get_int32().value; //get total answer count
	if (total_answer_count != 0)
	{
		average_answer_time = (float)user["statistics"]["total_answer_time"].get_int32().value / total_answer_count; //calculate
	}
	return average_answer_time;
}

/*
update player's total answer time.
input: player username, amount of additional time.
output: none.
*/
void MongoDatabase::incrementPlayerTotalAnswerTime(const std::string& username, const unsigned int answer_time)
{
	mongocxx::collection users = this->_db["users"];
	bsoncxx::builder::stream::document query{};
	bsoncxx::builder::stream::document update{};

	//build documents
	query << "username" << username;
	update << "$inc" <<  bsoncxx::builder::stream::open_document << "statistics.total_answer_time" << (int)answer_time << bsoncxx::builder::stream::close_document;

	users.find_one_and_update(query.view(), update.view()); //update
}

/*
get the player's correct answer count.
input: player username.
output: correct answer count.
*/
unsigned int MongoDatabase::getNumOfCorrectAnswers(const std::string& username) const
{
	mongocxx::collection users = this->_db["users"];

	auto cursor = users.find_one(make_document(kvp("username", username))); //find user document
	bsoncxx::document::view user = cursor.get().view(); //view document

	return (unsigned int)user["statistics"]["correct_answer_count"].get_int32().value;
}

/*
update the player's correct answer count.
input: player username.
output: none.
*/
void MongoDatabase::incrementNumOfCorrectAnswers(const std::string& username, unsigned int correctAnswerCount)
{
	mongocxx::collection users = this->_db["users"];
	bsoncxx::builder::stream::document query{};
	bsoncxx::builder::stream::document update{};

	//build documents
	query << "username" << username;
	update << "$inc" << bsoncxx::builder::stream::open_document << "statistics.correct_answer_count" << (int)correctAnswerCount << bsoncxx::builder::stream::close_document;

	users.find_one_and_update(query.view(), update.view()); //update
}

/*
update the player's total answer count.
input: player username.
output: none.
*/
unsigned int MongoDatabase::getNumOfWrongAnswers(const std::string& username) const
{
	mongocxx::collection users = this->_db["users"];

	auto cursor = users.find_one(make_document(kvp("username", username))); //find user document
	bsoncxx::document::view user = cursor.get().view(); //view document

	return (unsigned int)user["statistics"]["wrong_answer_count"].get_int32().value;
}

/*
update the player's total answer count.
input: player username.
output: none.
*/
void MongoDatabase::incrementNumOfWrongAnswers(const std::string& username, unsigned int wrongAnswerCount)
{
	mongocxx::collection users = this->_db["users"];
	bsoncxx::builder::stream::document query{};
	bsoncxx::builder::stream::document update{};

	//build documents
	query << "username" << username;
	update << "$inc" << bsoncxx::builder::stream::open_document << "statistics.wrong_answer_count" << (int)wrongAnswerCount << bsoncxx::builder::stream::close_document;

	users.find_one_and_update(query.view(), update.view()); //update
}

/*
update the player's game count.
input: player username.
output: none.
*/
unsigned int MongoDatabase::getNumOfPlayerGames(const std::string& username) const
{
	mongocxx::collection users = this->_db["users"];

	auto cursor = users.find_one(make_document(kvp("username", username))); //find user document
	bsoncxx::document::view user = cursor.get().view(); //view document

	return (unsigned int)user["statistics"]["game_count"].get_int32().value;
}

/*
update the player's game count.
input: player username.
output: none.
*/
void MongoDatabase::incrementNumOfPlayerGames(const std::string& username)
{
	mongocxx::collection users = this->_db["users"];
	bsoncxx::builder::stream::document query{};
	bsoncxx::builder::stream::document update{};

	//build documents
	query << "username" << username;
	update << "$inc" << bsoncxx::builder::stream::open_document << "statistics.game_count" << 1 << bsoncxx::builder::stream::close_document;

	users.find_one_and_update(query.view(), update.view()); //update
}
