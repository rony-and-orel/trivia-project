#pragma once
#include "IRequestHandler.h"
#include "SqliteDatabase.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
private:
	RequestHandlerFactory* m_handlerFactory;

	RequestResult signup(const RequestInfo& info);
	RequestResult login(const RequestInfo& info);
public:
	LoginRequestHandler(RequestHandlerFactory* handleFactory);
	~LoginRequestHandler() = default;
	bool isRequestRelevant(const MessageCode code) override;
	RequestResult handleRequest(const RequestInfo& info) override;
	void disconnect() override;
};

