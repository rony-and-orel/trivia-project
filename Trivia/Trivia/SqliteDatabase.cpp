#include "SqliteDatabase.h"

/*
callback function to receive data from the sqlite3 library. runs for every row of results in a query.
input: pointer to data object, amount of columns in result, values of the columns, names of the columns.
output: 0.
*/
int callback(void* data, int argc, char** argv, char** azColName)
{
	std::vector<std::unordered_map<std::string, std::string>>* results = static_cast<std::vector<std::unordered_map<std::string, std::string>>*>(data);
	results->emplace_back();

	std::unordered_map<std::string, std::string>& result = results->back();
	result.reserve(argc);

	for (int i = 0; i < argc; i++)
	{
		result[azColName[i]] = argv[i];
	}
	return 0;
}

/*
executes an sql query, specifically 'select' queries as it can return their results.
input: query.
output: a vector of all the results, inside every vector cell there's a map with all the columns of the result as keys.
*/
std::vector<std::unordered_map<std::string, std::string>> SqliteDatabase::runSelect(const std::string&& query) const
{
	std::vector<std::unordered_map<std::string, std::string>> results;
	sqlite3_exec(this->_db, query.c_str(), callback, &results, 0);
	return results;
}

/*
executes an sql query.
input: query.
output: none.
*/
void SqliteDatabase::execute(const std::string&& query)
{
	sqlite3_exec(this->_db, query.c_str(), 0, 0, 0);
}

//C'tor
SqliteDatabase::SqliteDatabase(const std::string dbFilename)
{
	const char* initDB = "CREATE TABLE USERS (USERNAME TEXT NOT NULL UNIQUE, SALT TEXT NOT NULL, HASH TEXT NOT NULL, EMAIL TEXT NOT NULL, ADDRESS TEXT NOT NULL, PHONE TEXT NOT NULL, BIRTH_DATE TEXT NOT NULL, CORRECT_ANSWER_COUNT INTEGER NOT NULL, WRONG_ANSWER_COUNT INTEGER NOT NULL, TOTAL_ANSWER_TIME INTEGER NOT NULL, GAME_COUNT INTEGER NOT NULL);";
	
	int fileExists = _access(dbFilename.c_str(), 0);

	if (sqlite3_open(dbFilename.c_str(), &this->_db) != SQLITE_OK) //if couldn't open the database file
	{
		this->_db = nullptr;
	}
	else if (fileExists == -1) //if the database file is new, try to initialize it.
	{
		sqlite3_exec(this->_db, initDB, 0, 0, 0);
	}
}

//D'tor
SqliteDatabase::~SqliteDatabase()
{
	sqlite3_close(this->_db);
	this->_db = nullptr;
}

/*
checks if a user exists.
input: username.
output: true if user exists, else false.
*/
bool SqliteDatabase::doesUserExist(const std::string& username) const
{
	return runSelect("SELECT 1 FROM USERS WHERE USERNAME = '" + username + "' LIMIT 1;").size();
}

/*
Checks if a user has a certain password.
input: username and password to check.
output: true is a user exists with that username and password, else false.
*/
bool SqliteDatabase::doesPasswordMatch(const std::string& username, const std::string& password) const
{
	auto data = runSelect("SELECT SALT, HASH FROM USERS WHERE USERNAME = '" + username + "' LIMIT 1;")[0];
	return Protector::compareHash(password, data["SALT"], Protector::toBinary(data["HASH"]));
}

/*
adds a new user to the database. note: username password and email mustn't be empty.
input: username, password, email, address, phone number and birth date of new user.
output: none.
*/
void SqliteDatabase::addNewUser(const std::string& username, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birthDate)
{
	Protector::Hash hash = Protector::generateNewHash(password);
	execute("INSERT INTO USERS VALUES ('" + username + "', '" + hash.salt + "', '" + Protector::toStr(hash.digest) + "', '" + email + "', '" + address + "', '" + phone + "', '" + birthDate + "', 0, 0, 0, 0);");
}

/*
gets all possible categories for questions.
input: none.
output: vector of all of the categories.
*/
std::vector<std::string> SqliteDatabase::getCategories() const
{
	std::vector<std::string> cats;
	auto results = runSelect("select distinct category from questions;");
	cats.reserve(results.size());
	for (auto& result : results)
	{
		cats.emplace_back(result["category"]);
	}
	std::sort(cats.begin(), cats.end());
	return cats;
}

/*
gets questions from the database.
input: how many questions are needed, difficulty, optional question category.
output: queue of the amount of questions requested, or empty if none matching found.
*/
std::queue<Question> SqliteDatabase::getQuestions(unsigned short count, byte diff, const std::string& category) const
{
	std::queue<Question> questions;
	std::vector<std::unordered_map<std::string, std::string>> results = runSelect("SELECT * FROM questions WHERE " + (diff <= TWO ? "difficulty = " + std::to_string(diff) : "1") + ((category == "" || category == "Any") ? "" : " AND category = '" + category + "'") + " ORDER BY RANDOM() LIMIT " + std::to_string(count) + ';');
	
	for (auto& question : results)
	{
		std::array<std::string, THREE> wrong_ans{ question["wrong_ans_1"], question["wrong_ans_2"], question["wrong_ans_3"] };
		questions.emplace(question["question"], wrong_ans, question["answer"]);
	}
	
	return questions;
}

/*
gets the total amount of time a player has taken to answer by given amount.
input: username.
output: average time it took a player to answer.
*/
float SqliteDatabase::getPlayerAverageAnswerTime(const std::string& username) const
{
	unsigned int total = getNumOfCorrectAnswers(username) + getNumOfWrongAnswers(username);
	return total == 0 ? 0 : (float)std::stoul(runSelect("SELECT TOTAL_ANSWER_TIME FROM USERS WHERE USERNAME = '" + username + "';")[0]["TOTAL_ANSWER_TIME"]) / total;
}

/*
increments the total amount of time a player has taken to answer by given amount.
input: username, time to add to total.
output: none.
*/
void SqliteDatabase::incrementPlayerTotalAnswerTime(const std::string& username, const unsigned int answer_time)
{
	execute("UPDATE USERS SET TOTAL_ANSWER_TIME = TOTAL_ANSWER_TIME + " + std::to_string(answer_time) + " WHERE USERNAME = '" + username + "';");
}

/*
gets the number of answers a player has answered correctly.
input: username.
output: amount of answers a player has answered correctly.
*/
unsigned int SqliteDatabase::getNumOfCorrectAnswers(const std::string& username) const
{
	return std::stoul(runSelect("SELECT CORRECT_ANSWER_COUNT FROM USERS WHERE USERNAME = '" + username + "';")[0]["CORRECT_ANSWER_COUNT"]);
}

/*
increments the number of correct answers a player has answered by 1.
input: username.
output: none.
*/
void SqliteDatabase::incrementNumOfCorrectAnswers(const std::string& username, unsigned int correctAnswerCount)
{
	execute("UPDATE USERS SET CORRECT_ANSWER_COUNT = CORRECT_ANSWER_COUNT + " + std::to_string(correctAnswerCount) + " WHERE USERNAME = '" + username +"';");
}

/*
gets number of answers a player has answered.
input: username.
output: amount of answers a player has answered.
*/
unsigned int SqliteDatabase::getNumOfWrongAnswers(const std::string& username) const
{
	return std::stoul(runSelect("SELECT WRONG_ANSWER_COUNT FROM USERS WHERE USERNAME = '" + username + "';")[0]["WRONG_ANSWER_COUNT"]);
}

/*
increments the number of answers a player has answered by 1.
input: username.
output: none.
*/
void SqliteDatabase::incrementNumOfWrongAnswers(const std::string& username, unsigned int wrongAnswerCount)
{
	execute("UPDATE USERS SET WRONG_ANSWER_COUNT = WRONG_ANSWER_COUNT + " + std::to_string(wrongAnswerCount) + " WHERE USERNAME = '" + username + "';");
}

/*
gets number of game a player has played.
input: username.
output: number of games a player has played.
*/
unsigned int SqliteDatabase::getNumOfPlayerGames(const std::string& username) const
{
	return std::stoul(runSelect("SELECT GAME_COUNT FROM USERS WHERE USERNAME = '" + username + "';")[0]["GAME_COUNT"]);
}

/*
increments the games a player has played by 1.
input: username.
output: none.
*/
void SqliteDatabase::incrementNumOfPlayerGames(const std::string& username)
{
	execute("UPDATE USERS SET GAME_COUNT = GAME_COUNT + 1 WHERE USERNAME = '" + username + "';");
}
