#include "RoomRequestHandler.h"

//C'tor
RoomRequestHandler::RoomRequestHandler(unsigned int roomid, const LoggedUser& user, RequestHandlerFactory* handlerFactory) :
	m_roomid(roomid), m_user(user), m_handlerFactory(handlerFactory)
{}
RoomRequestHandler::RoomRequestHandler(unsigned int roomid, LoggedUser&& user, RequestHandlerFactory* handlerFactory) :
	m_roomid(roomid), m_user(std::move(user)), m_handlerFactory(handlerFactory)
{}

/*
check if request is relevant.
input: message code.
output: true/false statement.
*/
bool RoomRequestHandler::isRequestRelevant(const MessageCode code)
{
	return (code == MessageCode::GET_ROOM_INFO || code == MessageCode::LEAVE_ROOM ||
		code == MessageCode::PROMOTE || code == MessageCode::DEMOTE || code == MessageCode::INVITE ||
		code == MessageCode::UNINVITE || code == MessageCode::GET_RANK || code == MessageCode::CLOSE_ROOM ||
		code == MessageCode::START_GAME || code == MessageCode::CAN_PLAY || code == MessageCode::GET_QUESTION_CATEGORIES) &&
		m_handlerFactory->getLoginManager().isLoggedIn(m_user.getUsername());
}

/*
handles the request.
input: request information.
output: request result.
*/
RequestResult RoomRequestHandler::handleRequest(const RequestInfo& info)
{
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	switch ((MessageCode)info.buffer[0])
	{
	case MessageCode::START_GAME:
		result = startGame(info);
		break;
	case MessageCode::CLOSE_ROOM:
		result = closeRoom(info);
		break;
	case MessageCode::GET_ROOM_INFO:
		result = getRoomInfo(info);
		break;
	case MessageCode::GET_QUESTION_CATEGORIES:
		result = getQuestionCategories(info);
		break;
	case MessageCode::INVITE:
		result = invite(info);
		break;
	case MessageCode::UNINVITE:
		result = uninvite(info);
		break;
	case MessageCode::GET_RANK:
		result = getRank(info);
		break;
	case MessageCode::PROMOTE:
		result = promote(info);
		break;
	case MessageCode::DEMOTE:
		result = demote(info);
		break;
	case MessageCode::LEAVE_ROOM:
		result = leaveRoom(info);
		break;
	case MessageCode::CAN_PLAY:
		result = canPlay(info);
		break;
	}
	return result;
}

void RoomRequestHandler::disconnect()
{
	if (m_handlerFactory->getRoomManager().doesRoomExist(m_roomid) &&
		m_handlerFactory->getRoomManager()[m_roomid].getRoomData().curPlayers == 1) //if last player in room
	{
		m_handlerFactory->getRoomManager().remove(m_roomid);
	}
	else if(m_handlerFactory->getRoomManager().doesRoomExist(m_roomid))
	{
		m_handlerFactory->getRoomManager()[m_roomid].remove(m_user); //leave
	}
}

/*
handles the get room info request.
input: request information.
output: request result.
*/
RequestResult RoomRequestHandler::getRoomInfo(const RequestInfo& info)
{
	Room* room = nullptr;
	response::GetRoomInfo response = {
		STATUS_FAIL,
		RoomData(),
		vector<std::pair<string, PlayerRank>>()
	};

	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	if (m_handlerFactory->getRoomManager().doesRoomExist(m_roomid))
	{
		Room* room = &m_handlerFactory->getRoomManager()[m_roomid];

		response.status = STATUS_SUCCESS;
		//get room data
		response.room = (*room).getRoomData();

		for (const auto& user : (*room).getUsers())
		{
			response.players.emplace_back(user.first.getUsername(), user.second);
		}
		result.newHandler = m_handlerFactory->createRoomRequestHandler(m_roomid, std::move(m_user));
	}
	else //if the room wasn't found
	{
		result.newHandler = m_handlerFactory->createMenuRequestHandler(std::move(m_user)); //go back to menu
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);

	return result;
}

/*
handles the leave room request.
input: request information.
output: request result.
*/
RequestResult RoomRequestHandler::leaveRoom(const RequestInfo& info)
{
	response::LeaveRoom response = {
		STATUS_FAIL
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	if (m_handlerFactory->getRoomManager().doesRoomExist(m_roomid))
	{
		if (m_handlerFactory->getRoomManager()[m_roomid].getRoomData().curPlayers == 1) //if last player in room
		{
			m_handlerFactory->getRoomManager().remove(m_roomid);
		}
		else
		{
			m_handlerFactory->getRoomManager()[m_roomid].remove(m_user); //leave
		}
		response.status = STATUS_SUCCESS;
	}
	
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createMenuRequestHandler(std::move(m_user)); //go back to menu

	return result;
}

/*
handles the promote request.
input: request information.
output: request result.
*/
RequestResult RoomRequestHandler::promote(const RequestInfo& info)
{
	request::Promote request = { };
	response::Promote response = {
		STATUS_FAIL
	};

	RequestResult result = {
		vector<byte>(),
		nullptr
	};
	request = JsonRequestPacketDeserializer::deserializePromoteRequest(info.buffer);
	
	//check the rank of the current user and check if the target user exists in room
	if (Protector::isSafe(request.user) && m_handlerFactory->getRoomManager().doesRoomExist(m_roomid)
		&& m_handlerFactory->getRoomManager()[m_roomid].getRank(m_user) <= PlayerRank::ADMIN 
		&& m_handlerFactory->getRoomManager()[m_roomid].doesUserExist(LoggedUser(request.user)))
	{
		m_handlerFactory->getRoomManager()[m_roomid].promote(LoggedUser(request.user)); //promote
		response.status = STATUS_SUCCESS;
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createRoomRequestHandler(m_roomid, std::move(m_user));

	return result;
}

/*
handles the demote request.
input: request information.
output: request result.
*/
RequestResult RoomRequestHandler::demote(const RequestInfo& info)
{
	request::Demote request = { };
	response::Demote response = {
		STATUS_FAIL
	};

	RequestResult result = {
		vector<byte>(),
		nullptr
	};
	request = JsonRequestPacketDeserializer::deserializeDemoteRequest(info.buffer);

	//check the rank of the current user and check if the target user exists in room
	if (Protector::isSafe(request.user) && m_handlerFactory->getRoomManager().doesRoomExist(m_roomid)
		&& m_handlerFactory->getRoomManager()[m_roomid].getRank(m_user) <= PlayerRank::ADMIN
		&& m_handlerFactory->getRoomManager()[m_roomid].doesUserExist(LoggedUser(request.user)))
	{
		m_handlerFactory->getRoomManager()[m_roomid].demote(LoggedUser(request.user)); //demote
		response.status = STATUS_SUCCESS;
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createRoomRequestHandler(m_roomid, std::move(m_user));

	return result;
}

/*
handles the invite request.
input: request information.
output: request result.
*/
RequestResult RoomRequestHandler::invite(const RequestInfo& info)
{
	response::Invite response = {
		STATUS_FAIL
	};

	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	request::Invite request = JsonRequestPacketDeserializer::deserializeInviteRequest(info.buffer);

	//check the rank of the current user and check if the target user is logged in
	if (Protector::isSafe(request.user) && m_handlerFactory->getRoomManager().doesRoomExist(m_roomid)
		&& m_handlerFactory->getRoomManager()[m_roomid].getRank(m_user) <= PlayerRank::ADMIN
		&& m_handlerFactory->getLoginManager().doesExist(request.user)
		&& m_handlerFactory->getRoomManager()[m_roomid].invite(LoggedUser(std::move(request.user)))) //invite
	{
		response.status = STATUS_SUCCESS;
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createRoomRequestHandler(m_roomid, std::move(m_user));

	return result;
}

/*
handles the uninvite request.
input: request information.
output: request result.
*/
RequestResult RoomRequestHandler::uninvite(const RequestInfo& info)
{
	response::Uninvite response = {
		STATUS_FAIL
	};

	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	request::Uninvite request = JsonRequestPacketDeserializer::deserializeUninviteRequest(info.buffer);
	
	//check the rank of the current user and check if the target user exists in room
	if (Protector::isSafe(request.user) && m_handlerFactory->getRoomManager().doesRoomExist(m_roomid)
		&& m_handlerFactory->getRoomManager()[m_roomid].getRank(m_user) <= PlayerRank::ADMIN
		&& m_handlerFactory->getRoomManager()[m_roomid].doesUserExist(LoggedUser(request.user))
		&& m_handlerFactory->getRoomManager()[m_roomid].uninvite(LoggedUser(std::move(request.user)))) //uninvite
	{
		response.status = STATUS_SUCCESS;
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createRoomRequestHandler(m_roomid, std::move(m_user));

	return result;
}

/*
handles the get rank request.
input: request information.
output: request result.
*/
RequestResult RoomRequestHandler::getRank(const RequestInfo& info)
{
	response::GetRank response = {
		STATUS_FAIL,
		PlayerRank()
	};

	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	if (m_handlerFactory->getRoomManager().doesRoomExist(m_roomid))
	{
		response.rank = m_handlerFactory->getRoomManager()[m_roomid].getRank(m_user); //get rank
		response.status = STATUS_SUCCESS;
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createRoomRequestHandler(m_roomid, std::move(m_user));

	return result;
}

/*
handles the close room request.
input: request information.
output: request result.
*/
RequestResult RoomRequestHandler::closeRoom(const RequestInfo& info)
{
	response::CloseRoom response = {
		STATUS_FAIL
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	//check the rank of the current user and the room existence
	if (m_handlerFactory->getRoomManager().doesRoomExist(m_roomid) 
		&& m_handlerFactory->getRoomManager()[m_roomid].getRank(m_user) <= PlayerRank::CREATOR
		&& m_handlerFactory->getRoomManager().remove(m_roomid)) //erase the room
	{
		response.status = STATUS_SUCCESS;
		result.newHandler = m_handlerFactory->createMenuRequestHandler(std::move(m_user)); //go back to menu
	}
	else
	{
		result.newHandler = m_handlerFactory->createRoomRequestHandler(m_roomid, std::move(m_user));
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

/*
handles the start game request.
input: request information.
output: request result.
*/
RequestResult RoomRequestHandler::startGame(const RequestInfo& info)
{
	response::StartGame response = {
		STATUS_FAIL
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	request::StartGame request = JsonRequestPacketDeserializer::deserializeStartGameRequest(info.buffer);

	//try to create game
	if (Protector::isSafe(request.category) && request.timePerQuestion >= MIN_TIME_PER_QUESTION && request.roundCount >= 1
		&& m_handlerFactory->getRoomManager().doesRoomExist(m_roomid)
		&& m_handlerFactory->getRoomManager().createGame(m_handlerFactory->getRoomManager()[m_roomid], request.timePerQuestion, request.roundCount, request.diff, request.category))
	{
		response.status = STATUS_SUCCESS;
		result.newHandler = m_handlerFactory->createGameRequestHandler(m_handlerFactory->getRoomManager()[m_roomid], std::move(m_user)); //go to game
	}
	else
	{
		result.newHandler = m_handlerFactory->createRoomRequestHandler(m_roomid, std::move(m_user)); //stay in room
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

/*
handles the get question categories request.
input: request information.
output: request result.
*/
RequestResult RoomRequestHandler::getQuestionCategories(const RequestInfo& info)
{
	response::GetQuestionCategories response = {
		std::vector<std::string>()
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	response.categories = m_handlerFactory->getRoomManager().getQuestionCategories(); //get categories

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = m_handlerFactory->createRoomRequestHandler(m_roomid, std::move(m_user)); //set new room request handler

	return result;
}

RequestResult RoomRequestHandler::canPlay(const RequestInfo& info)
{
	response::CanPlay response = {
		STATUS_FAIL
	};
	RequestResult result = {
		vector<byte>(),
		nullptr
	};

	if (m_handlerFactory->getRoomManager().doesRoomExist(m_roomid))
	{
		if (m_handlerFactory->getRoomManager()[m_roomid].isGameRunning())
		{
			response.status = STATUS_SUCCESS;
			result.newHandler = m_handlerFactory->createGameRequestHandler(m_handlerFactory->getRoomManager()[m_roomid], std::move(m_user)); //go to game
		}
		else
		{
			result.newHandler = m_handlerFactory->createRoomRequestHandler(m_roomid, std::move(m_user)); //set new room request handler
		}
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);

	return result;
}