#pragma once
#include <vector>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <chrono>
#include <mutex>
#include <thread>
#include "Definitions.h"
#include "Question.h"
#include "LoggedUser.h"

class Game
{
private:
	GameData m_metadata;
	unsigned short m_playerCloseCount;
	std::queue<Question> m_questions;
	std::unordered_map<LoggedUser, GameStatistics> m_players;
	mutable std::chrono::steady_clock::time_point m_startTime; //time stored here when a question is given. used to check time player took to answer
	mutable bool m_timeStarted; //set to true when started question timer; set to false when starting moving to next question
	mutable std::mutex m_mutex;
	std::thread* m_timer;

	void advanceQuestion();
	void timer();
	void killTimer();
	void startTimer();
public:
	explicit Game(const std::queue<Question>& questions, const std::unordered_set<LoggedUser>& users, unsigned short timePerQuestion);
	Game(const Game&) = delete;
	~Game();
	const std::pair<unsigned short, const Question&> getQuestion() const;
	bool isGameOver() const;
	bool submitAnswer(unsigned short id, const LoggedUser& user);
	unsigned short getPlayersLeft();
	unsigned short getTimeSinceStart() const;
	const GameData& getGameData() const;
	const std::unordered_map<LoggedUser, GameStatistics>& getPlayers() const;

	friend class Room;
};
