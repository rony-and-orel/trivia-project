#pragma once
#include <sqlite3.h>
#include <vector>
#include <array>
#include <unordered_map>
#include <algorithm>
#include <io.h>
#include "IDatabase.h"

class SqliteDatabase : public IDatabase
{
private:
	sqlite3* _db;

	friend int callback(void* data, int argc, char** argv, char** azColName);
	std::vector<std::unordered_map<std::string, std::string>> runSelect(const std::string&& query) const;
	void execute(const std::string&& query);
public:
	explicit SqliteDatabase(const std::string dbFilename);
	SqliteDatabase(const SqliteDatabase&) = delete;
	~SqliteDatabase();

	bool doesUserExist(const std::string& username) const override;
	bool doesPasswordMatch(const std::string& username, const std::string& password) const override;
	void addNewUser(const std::string& username, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birthDate) override;

	std::vector<std::string> getCategories() const override;
	std::queue<Question> getQuestions(unsigned short count, byte diff, const std::string& category = "") const override;

	float getPlayerAverageAnswerTime(const std::string& username) const override;
	void incrementPlayerTotalAnswerTime(const std::string& username, unsigned int answerTime) override;

	unsigned int getNumOfCorrectAnswers(const std::string& username) const override;
	void incrementNumOfCorrectAnswers(const std::string& username, unsigned int correctAnswerCount) override;

	unsigned int getNumOfWrongAnswers(const std::string& username) const override;
	void incrementNumOfWrongAnswers(const std::string& username, unsigned int wrongAnswerCount) override;

	unsigned int getNumOfPlayerGames(const std::string& username) const override;
	void incrementNumOfPlayerGames(const std::string& username) override;
};