#pragma once
#include <unordered_set>
#include <regex>
#include <mutex>
#include "LoggedUser.h"
#include "IDatabase.h"

class LoginManager
{
private:
	IDatabase* m_database;
	std::unordered_set<LoggedUser> m_loggedUsers;
	std::mutex m_mutex;
public:
	explicit LoginManager(IDatabase* database);
	LoginManager(const LoginManager&) = delete;
	~LoginManager() = default;
	bool signup(const std::string& username, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birthDate);
	bool login(const std::string& username, const std::string& password);
	bool logout(const std::string& username);
	bool isLoggedIn(const std::string& username) const;
	bool doesExist(const std::string& username) const;
};
