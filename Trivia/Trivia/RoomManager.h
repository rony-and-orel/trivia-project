#pragma once
#include <unordered_map>
#include <algorithm>
#include <mutex>
#include "IDatabase.h"
#include "Room.h"

class RoomManager
{
private:
	std::unordered_map<unsigned int, Room> m_rooms; //Room id, Room object
	IDatabase* m_database;
	unsigned int m_nextid;
	std::mutex m_mutex;
public:
	explicit RoomManager(IDatabase* database);
	RoomManager(const RoomManager&) = delete;
	~RoomManager() = default;

	unsigned int create(const LoggedUser& creator, const std::string& name, bool ispublic, unsigned short maxPlayers);
	bool remove(unsigned int id);
	bool doesRoomExist(unsigned int id) const;

	std::vector<std::string> getQuestionCategories() const;
	bool createGame(Room& room, unsigned short timePerQuestion, unsigned short roundCount, byte diff, std::string category = "");
	void deleteGame(Room& room);

	const std::unordered_map<unsigned int, Room>& getRooms() const;
	Room& operator[](unsigned int id);
	const Room& operator[](unsigned int id) const;
};
