#include "RoomManager.h"

//C'tor
RoomManager::RoomManager(IDatabase* database) :
	m_database(database), m_nextid(1)
{}

/*
Creates a new Room.
input: Room details: ID, name, maximum amount of players, time given per question, if the room is active.
output: if operation was successful.
*/
unsigned int RoomManager::create(const LoggedUser& creator, const std::string& name, bool ispublic, unsigned short maxPlayers)
{
	std::unique_lock<std::mutex> lck(m_mutex);
	auto it = m_rooms.emplace(std::piecewise_construct, std::forward_as_tuple(m_nextid), std::forward_as_tuple(creator, m_nextid, name, ispublic, maxPlayers)).first;
	m_nextid++;
	return it == m_rooms.end() ? 0 : (*it).first;
}

/*
Deletes a Room.
input: Room's ID.
output: if operation was successful.
*/
bool RoomManager::remove(unsigned int id)
{
	std::unique_lock<std::mutex> lck(m_mutex);
	return m_rooms.erase(id);
}

/*
checks if a room exists.
input: room id.
output: if room exists.
*/
bool RoomManager::doesRoomExist(unsigned int id) const
{
	return m_rooms.find(id) != m_rooms.end();
}

//returns avaliable categories of questions
std::vector<std::string> RoomManager::getQuestionCategories() const
{
	return m_database->getCategories();
}

/*
creates a new game.
input: room object, max time per question, amount of rounds, min and max question difficulty, question category (optional).
output: none.
*/
bool RoomManager::createGame(Room& room, unsigned short timePerQuestion, unsigned short roundCount, byte diff, std::string category)
{
	bool success = false;
	auto convert = [&]() -> std::unordered_set<LoggedUser>
	{
		std::unordered_set<LoggedUser> users;
		users.reserve(room.getRoomData().curPlayers);
		for (auto& user : room.getUsers())
		{
			if (user.second != PlayerRank::INVITED)
			{
				users.emplace(user.first);
			}
		}
		return users;
	};
	room.m_lastGameResults.clear();
	if (room.m_game == nullptr)
	{
		std::unique_lock<std::mutex> lck(room.m_mutex);
		room.m_game = new Game(m_database->getQuestions(roundCount, diff, category), convert(), timePerQuestion);
		room.m_metadata.isGameRunning = true;
		success = true;
	}
	return success;
}

/*
deletes a game.
input: room with game to delete.
output: none.
*/
void RoomManager::deleteGame(Room& room)
{
	std::unique_lock<std::mutex> lck(room.m_mutex);
	if (room.m_game != nullptr)
	{
		for (const auto& player : room->getPlayers())
		{
			//push player results
			room.m_lastGameResults.push_back({ player.first.getUsername(),
										 player.second.correctAnswerCount,
										 player.second.wrongAnswerCount,
										 player.second.totalAnswerTime });
			//write in database
			m_database->incrementPlayerTotalAnswerTime(player.first.getUsername(), player.second.totalAnswerTime);
			m_database->incrementNumOfCorrectAnswers(player.first.getUsername(), player.second.correctAnswerCount);
			m_database->incrementNumOfWrongAnswers(player.first.getUsername(), player.second.wrongAnswerCount);
			m_database->incrementNumOfPlayerGames(player.first.getUsername());
		}
		delete room.m_game;
		room.m_game = nullptr;
		room.m_metadata.isGameRunning = false;
	}
}

/*
makes and returns copies of all the Rooms in unordered_map form.
input: none.
output: Room unordered_map.
*/
const std::unordered_map<unsigned int, Room>& RoomManager::getRooms() const
{
	return this->m_rooms;
}

//for accessing rooms in non-const manner
Room& RoomManager::operator[](unsigned int id)
{
	return (*this->m_rooms.find(id)).second;
}

//for accessing rooms in const manner
const Room& RoomManager::operator[](unsigned int id) const
{
	return (*this->m_rooms.find(id)).second;
}
