#include "StatisticsManager.h"

//C'tor
StatisticsManager::StatisticsManager(IDatabase* database) :
	m_database(database)
{
}

/*
Collects and returns statistics about a user.
input: user object.
output: vector of statistics:
*/
std::vector<std::string> StatisticsManager::getStatistics(const LoggedUser& user) const
{
	std::vector<std::string> stats;
	std::string username = user.getUsername();
	stats.reserve(STAT_COUNT);
	stats.push_back(std::to_string(m_database->getNumOfPlayerGames(username)));
	stats.push_back(std::to_string(m_database->getNumOfCorrectAnswers(username)));
	stats.push_back(std::to_string(m_database->getNumOfWrongAnswers(username)));
	stats.push_back(std::to_string(m_database->getPlayerAverageAnswerTime(username)));
	return stats;
}

/*
Updates player's statistics from a game, into the database (use ONLY after the game has finished).
input: game object.
output: none.
*/
void StatisticsManager::updateStatistics(const Game& game)
{
	for (auto& user : game.getPlayers())
	{
		m_database->incrementNumOfCorrectAnswers(user.first.getUsername(), user.second.correctAnswerCount);
		m_database->incrementNumOfPlayerGames(user.first.getUsername());
		m_database->incrementNumOfWrongAnswers(user.first.getUsername(), user.second.wrongAnswerCount);
		m_database->incrementPlayerTotalAnswerTime(user.first.getUsername(), user.second.totalAnswerTime);
	}
}
