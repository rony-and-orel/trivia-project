﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WPF_Client
{
    public partial class SignupPage : Page
    {
        private bool passwordDefault = true;
        private bool usernameDefault = true;
        private bool emailDefault = true;
        private bool phoneDefault = true;
        private bool addressDefault = true;
        private bool dateDefault = true;

        public SignupPage()
        {
            InitializeComponent();

            ErrorGrid.Visibility = Visibility.Hidden;
            FormatGrid.Visibility = Visibility.Hidden;
        }

        /*
         * SignupButton click event
         */
        private void SignupButton_Click(object sender, RoutedEventArgs e)
        {
            if (!passwordDefault && !usernameDefault && !emailDefault && !phoneDefault && !addressDefault && !dateDefault)
            {
                if (Global.ClientCommunicator.Signup(UsernameField.Text,
                                        PasswordField.Text,
                                        EmailField.Text,
                                        AddressField.Text,
                                        PhoneField.Text,
                                        BirthDateField.Text)) //signup
                {
                    this.NavigationService.Navigate(new MenuPage(false), UriKind.Relative); //go to menu page
                }
                else if (Global.ClientCommunicator.HasErrorOccurred())
                {
                    //reload
                    var page = (Page)this;
                    Global.ClientCommunicator.Reload(ref page);
                }
                else
                {
                    ErrorGrid.Visibility = Visibility.Visible; //show error message
                }
            }
        }

        /* Textbox focus start */
        private void UsernameField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref UsernameField, ref usernameDefault);
        }

        private void UsernameField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref UsernameField, ref usernameDefault, "Username...");
        }

        private void PasswordField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref PasswordField, ref passwordDefault);
        }

        private void PasswordField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref PasswordField, ref passwordDefault, "Password...");
        }

        private void EmailField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref EmailField, ref emailDefault);
        }

        private void EmailField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref EmailField, ref emailDefault, "Email...");
        }

        private void AddressField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref AddressField, ref addressDefault);
        }

        private void AddressField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref AddressField, ref addressDefault, "Address...");
        }

        private void PhoneField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref PhoneField, ref phoneDefault);
        }

        private void PhoneField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref PhoneField, ref phoneDefault, "Phone...");
        }

        private void BirthDateField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref BirthDateField, ref dateDefault);
        }

        private void BirthDateField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref BirthDateField, ref dateDefault, "Birth Date...");
        }
        /* Textbox focus end */

        /*
         * GoBackButton click event
         */
        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new LoginPage(), UriKind.Relative); //go back to login page
        }

        /* 
         * OkayButton click event.
         */
        private void OkayButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorGrid.Visibility = Visibility.Hidden;
        }

        private void OkayButtonFormat_Click(object sender, RoutedEventArgs e)
        {
            FormatGrid.Visibility = Visibility.Hidden;
        }

        private void FormatButton_Click(object sender, RoutedEventArgs e)
        {
            FormatGrid.Visibility = Visibility.Visible;
        }
    }
}
