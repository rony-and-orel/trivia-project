﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WPF_Client
{
    public partial class EndGamePage : Page
    {
        Response.GetGameResults results; //game results
        List<Tuple<string, uint>> podium; //game podium
        private Response.GetGameInfo info; //game info

        public EndGamePage(Response.GetGameResults gameResults, Response.GetGameInfo gameInfo)
        {
            InitializeComponent();
            //set
            podium = new List<Tuple<string, uint>>();
            results = gameResults;
            info = gameInfo;
            //init display
            initPodium();
            view();
        }
        /*
         * view the menu.
         * input: none.
         * output: none.
         */
        private void view()
        {
            MenuPanel.Visibility = Visibility.Visible;
            PodiumView.Visibility = Visibility.Hidden;
            ResultsView.Visibility = Visibility.Hidden;
            //set position display
            PositionLabel.Content = "#" + (podium.FindIndex(x => x.Item1 == Global.ClientCommunicator.GetUserName()) + 1).ToString();
        }
        /*
         * initiates the podium.
         * input: none.
         * output: none.
         */
        private void initPodium()
        {
            foreach (Response.PlayerResults result in results.results) //for each result
            {
                //insert
                podium.Add(new Tuple<string, uint>(result.username, calculateScore(result.correct_answer_count, result.wrong_answer_count, result.total_answer_time)));
            }
            //sort results
            podium.Sort(delegate (Tuple<string, uint> a, Tuple<string, uint> b)
            {
                return b.Item2.CompareTo(a.Item2);
            });
        }
        /*
         * calculates the player score.
         * input: wrong and correct answer count, total answer time.
         */
        private uint calculateScore(uint correct_answer_count, uint wrong_answer_count, uint total_answer_time)
        {
            return (uint)(info.timeout - ((float)total_answer_time / (correct_answer_count + wrong_answer_count)))*(correct_answer_count)*10;
        }
        /*
         * PodiumDisplayButton click event
         */
        private void PodiumDisplayButton_Click(object sender, RoutedEventArgs e)
        {
            int pos = 1;
            TextBlock player = null;

            MenuPanel.Visibility = Visibility.Hidden; //hide
            
            //clear
            PodiumDisplay.Items.Clear();
            foreach (Tuple<string, uint> place in podium) //for each player in podium
            {
                player = new TextBlock();
                //set player display in podium
                player.Text += "#" + pos.ToString() + "  " + place.Item1 + "\n";
                player.Text = player.Text.PadRight(10); //format
                player.FontFamily = new FontFamily("Impact");
                player.Foreground = Brushes.White;
                player.FontSize = 48;
                //add to display
                PodiumDisplay.Items.Add(player);
                pos++;
            }
            PodiumView.Visibility = Visibility.Visible;
        }
        /*
         * ResultsButton click event
         */
        private void ResultsButton_Click(object sender, RoutedEventArgs e)
        {
            Response.PlayerResults result = results.results.Find(x => x.username == Global.ClientCommunicator.GetUserName()); //find user result
            MenuPanel.Visibility = Visibility.Hidden;
            ResultsView.Visibility = Visibility.Visible;

            //set player result display
            ScoreCount.Content = calculateScore(result.correct_answer_count, result.wrong_answer_count, result.total_answer_time).ToString();
            CorrectCount.Content = result.correct_answer_count.ToString();
            WrongCount.Content = result.wrong_answer_count.ToString();
            TimeCount.Content = (result.correct_answer_count + result.wrong_answer_count) != 0 ? (result.total_answer_time / (double)(result.correct_answer_count + result.wrong_answer_count)).ToString() : info.timeout.ToString();
        }
        /*
         * ReplayButton click event
         */
        private void ReplayButton_Click(object sender, RoutedEventArgs e)
        {
            if(Global.ClientCommunicator.CanPlay()) //if new game started while player is in endgame page
            {
                this.NavigationService.Navigate(new GamePage(), UriKind.Relative); //go to the game page
            }
            else if(Global.ClientCommunicator.GetRank() == PlayerRank.CREATOR) //check rank
            {
                this.NavigationService.Navigate(new RoomPage(), UriKind.Relative); //go to the room page
            }
            else if (Global.ClientCommunicator.HasErrorOccurred())
            {
                //reload
                var page = (Page)this;
                Global.ClientCommunicator.Reload(ref page);
            }
            else
            {
                this.NavigationService.Navigate(new WaitPage(), UriKind.Relative); //go to the wait page
            }
        }
        /*
        * ReturnButton click event
        */
        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            if(Global.ClientCommunicator.LeaveRoom()) //leave
            {
                this.NavigationService.Navigate(new MenuPage(false), UriKind.Relative); //go to the menu page
            }
            else if (Global.ClientCommunicator.HasErrorOccurred())
            {
                var page = (Page)this;
                Global.ClientCommunicator.Reload(ref page);
            }
        }
        /*
        * GoBackButton1 click event
        */
        private void GoBackButton1_Click(object sender, RoutedEventArgs e)
        {
            MenuPanel.Visibility = Visibility.Visible;
            ResultsView.Visibility = Visibility.Hidden;
        }
        /*
        * GoBackButton2 click event
        */
        private void GoBackButton2_Click(object sender, RoutedEventArgs e)
        {
            MenuPanel.Visibility = Visibility.Visible;
            PodiumView.Visibility = Visibility.Hidden;
        }
    }
}
