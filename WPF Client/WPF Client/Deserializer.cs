﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace WPF_Client
{
    /*
    * room metadata class.
    */
    public class RoomData
    {
        public int id { get; set; }
        public string name { get; set; }
        public ushort max_players { get; set; }
        public ushort cur_players { get; set; }
        public bool is_public { get; set; }
        public bool is_game_running { get; set; }
    };

    namespace Response
    {
        /*
         * Error response data class.
         */
        class Error
        {
            public string message { get; set; }
        }
        
        /*
         * Signup response data class.
         */
        class Signup
        {
            public StatusCode status { get; set; }
        }

        /*
        * Login response data class.
        */
        class Login
        {
            public StatusCode status { get; set; }
        }

        /*
         * Logout response data class.
         */
        class Logout
        {
            public StatusCode status { get; set; }
        }

        /*
         * Get statistics response data class.
         */
        public class GetStatistics
        {
            public string games_played { get; set; }
            public string correct_answers { get; set; }
            public string wrong_answers { get; set; }
            public string avg_time_per_answer { get; set; }
        }

        /*
         * Get rooms response data class.
         */
        public class GetRooms
        {
            public List<RoomData> rooms { get; set; }
        };
        
        /*
         * player info data struct.
         */
        public struct playerInfo
        {
            public string username { get; set; }
            public PlayerRank state { get; set; }
        };
        
        /*
         * GetRoomInfo response data class.
         */
        public class GetRoomInfo
        {
            public StatusCode status { get; set; }
            public RoomData room { get; set; }
            public List<playerInfo> players { get; set; }
        };

        /*
         * Create room response data class.
         */
        class CreateRoom
        {
            public StatusCode status { get; set; }
        }

        /*
         * Join room response data class.
         */
        class JoinRoom
        {
            public StatusCode status { get; set; }
        }

        /*
         * Leave room response data class.
         */
        class LeaveRoom
        {
            public StatusCode status { get; set; }
        };

        /*
         * Close room response data class.
         */
        class CloseRoom
        {
            public StatusCode status { get; set; }
        };

        /*
         * Promote response data class.
         */
        class Promote
        {
            public StatusCode status { get; set; }
        };

        /*
         * Demote response data class.
         */
        class Demote
        {
            public StatusCode status { get; set; }
        };

        /*
         * Invite response data class.
         */
        class Invite
        {
            public StatusCode status { get; set; }
        };

        /*
         * Invite response data class.
         */
        class Uninvite
        {
            public StatusCode status { get; set; }
        };

        /*
         * Is admin response data class.
         */
        public class GetRank
        {
            public StatusCode status { get; set; }
            public PlayerRank rank { get; set; }
        };

        /*
         * Start game response data class.
         */
        class StartGame
        {
            public StatusCode status { get; set; }
        };

        /*
         * Can play response data class.
         */
        class CanPlay
        {
            public StatusCode status { get; set; }
        };

        /*
         * Get Question Categories response data class.
         */
        class GetQuestionCategories
        {
            public List<string> categories { get; set; }
        };

        /*
         * get question response data class.
         */
        public class GetQuestion
        {
            public StatusCode status { get; set; }
            public string question { get; set; }
            public ushort id { get; set; }
            public ushort time { get; set; }
            public List<string> answers { get; set; }
        };

        /*
         * Submit answer response data class.
         */
        public class SubmitAnswer
        {
            public StatusCode status { get; set; }
            public uint correct_answer_id { get; set; }
        };

        /*
         * player results data struct.
         */
        public struct PlayerResults
        {
            public string username { get; set; }
            public uint correct_answer_count { get; set; }
            public uint wrong_answer_count { get; set; }
            public uint total_answer_time { get; set; }
        };
        /*
         * Get game results response data class.
         */
        public class GetGameResults
        {
            public StatusCode status { get; set; }
            public List<PlayerResults> results { get; set; }
        };

        /*
         * Leave game response data class.
         */
        class LeaveGame
        {
            public StatusCode status { get; set; }
        };

        /*
         * get game info response data class.
         */
        public class GetGameInfo
        {
            public ushort timeout { get; set; }
            public uint question_amount { get; set; }
        };
    }
    
    class Deserializer
    {
        /*
         * resturns the size of the data field in the response.
         * input: message header.
         * output: size of data (in bytes).
         */
        public static uint GetDocumentLength(byte[] header)
        {
            return BitConverter.EndianBitConverter.LittleEndian.ToUInt32(header.Skip(1).Take(sizeof(uint)).ToArray(), 0);
        }

        /*
         * Checks if the data can be deserialized, if not thows an exception.
         * input: data buffer.
         * output: none.
         */
        public static void CanDeserialize(byte[] data)
        {
            if(data == null || data.Length == 0)
            {
                throw new System.Exception("data cannot be deserialized");
            }
        }

        /*
        * Deserializes the error response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.Error DeserializeErrorResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.Error>(Encoding.Default.GetString(data.ToArray()));
        }
        
        /*
        * Deserializes the signup response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.Signup DeserializeSignupResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.Signup>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
         * Deserializes the login response.
         * input: data byte array.
         * output: deserialized response.
         */
        public static Response.Login DeserializeLoginResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.Login>(Encoding.Default.GetString(data.ToArray()));
        }
        
        /*
        * Deserializes the logout response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.Logout DeserializeLogoutResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.Logout>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the get statistics response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.GetStatistics DeserializeGetStatisticsResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.GetStatistics>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the get rooms response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.GetRooms DeserializeGetRoomsResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.GetRooms>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the GetRoomInfo response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.GetRoomInfo DeserializeGetRoomInfoResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.GetRoomInfo>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the create room response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.CreateRoom DeserializeCreateRoomResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.CreateRoom>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the join room response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.JoinRoom DeserializeJoinRoomResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.JoinRoom>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the LeaveRoom response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.LeaveRoom DeserializeLeaveRoomResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.LeaveRoom>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the CloseRoom response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.CloseRoom DeserializeCloseRoomResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.CloseRoom>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the Promote response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.Promote DeserializeJoinPromoteResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.Promote>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the Demote response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.Demote DeserializeJoinDemoteResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.Demote>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the Invite response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.Invite DeserializeInviteResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.Invite>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the Uninvite response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.Uninvite DeserializeUninviteResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.Uninvite>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the getRank response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.GetRank DeserializeGetRankResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.GetRank>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the canPlay response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.CanPlay DeserializeCanPlayResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.CanPlay>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the StartGame response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.StartGame DeserializeStartGameResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.StartGame>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the Get Question Categories response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.GetQuestionCategories DeserializeGetQuestionCategoriesResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.GetQuestionCategories>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the Get Question response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.GetQuestion DeserializeGetQuestionResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.GetQuestion>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the SubmitAnswer response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.SubmitAnswer DeserializeSubmitAnswerResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.SubmitAnswer>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the GetGameResults response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.GetGameResults DeserializeGetGameResultsResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.GetGameResults>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
        * Deserializes the LeaveGame response.
        * input: data byte array.
        * output: deserialized response.
        */
        public static Response.LeaveGame DeserializeLeaveGameResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.LeaveGame>(Encoding.Default.GetString(data.ToArray()));
        }

        /*
       * Deserializes the GetGameInfo response.
       * input: data byte array.
       * output: deserialized response.
       */
        public static Response.GetGameInfo DeserializeGetGameInfoResponse(byte[] data)
        {
            CanDeserialize(data);
            return JsonConvert.DeserializeObject<Response.GetGameInfo>(Encoding.Default.GetString(data.ToArray()));
        }
    }
}
