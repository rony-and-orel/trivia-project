﻿using System.Threading;
using System.Windows.Media;
using System.Windows.Controls;

namespace WPF_Client
{
    enum Sizes : int
    {
        HEADER_SIZE = 5
    }

    enum MessageCode : byte
    {
		//error
		ERROR = 0,

		//login
		SIGNUP = 10,
		LOGIN = 11,
		LOGOUT = 12,

		//players
		GET_STATISTICS = 21, //of player who requested

		//rooms
		GET_ROOMS = 30,
		GET_ROOM_INFO = 31, //... of the room player is currently in

		CREATE_ROOM = 33,
		JOIN_ROOM = 34,
		LEAVE_ROOM = 35,
		CLOSE_ROOM = 36, //... that the player is currently in

		PROMOTE = 37,
		DEMOTE = 38,
		INVITE = 39,
		UNINVITE = 40,
		GET_RANK = 41,
		START_GAME = 42, //start running game
		GET_QUESTION_CATEGORIES = 43,
        CAN_PLAY = 44,

        //game
        GET_QUESTION = 50,
		SUBMIT_ANSWER = 51,
		GET_GAME_RESULTS = 52,
		LEAVE_GAME = 53,
        GET_GAME_INFO = 54,
    };

    public class DefaultValues
    {
        public const ushort QuestionsAmount =  10;
        public const ushort Timeout =  20;
        public const ushort UsersAmount =  2;
    }

    public enum StatusCode : byte
    {
        STATUS_SUCCESS = 1,
        STATUS_FAIL = 0
    };

	public enum PlayerRank : byte
	{
        CREATOR = 0,
        ADMIN = 1,
		MEMBER = 2,
		INVITED = 3,
        NONE = 4,
	};

	public enum CommunicatorErrorCode : byte
    {
        IRRELEVANT = 0,
        INTERNAL_SERVER_ERROR = 1,
        CONNECTION_ERROR = 2
    }

    public static class Global
    {
        public static Communicator ClientCommunicator = new Communicator();
        public static bool IsSinglePlay = false;

        public static SolidColorBrush DefaultTextboxForegroundWhite = new SolidColorBrush(Color.FromArgb(131, 255, 255, 255));
        public static SolidColorBrush DefaultTextboxForegroundRed = new SolidColorBrush(Color.FromArgb(255, 255, 76, 94));

        /*
         * textbox focus function.
         * input: textbox, if default value flag.
         * output: none.
         */
        public static void TextboxFocus(ref TextBox textbox, ref bool isDefault)
        {
            if (isDefault)
            {
                textbox.Text = string.Empty;
                textbox.Foreground = Brushes.White;
                isDefault = false;
            }
        }

        /*
         * textbox unfocus function.
         * input: textbox, if default value flag, the default value for textbox.
         * output: none.
         */
        public static void TextboxUnfocus(ref TextBox textbox, ref bool isDefault, string defaultValue)
        {
            if (textbox.Text == "")
            {
                textbox.Text = defaultValue;
                textbox.Foreground = Global.DefaultTextboxForegroundWhite;
                isDefault = true;
            }
        }

    }
}
