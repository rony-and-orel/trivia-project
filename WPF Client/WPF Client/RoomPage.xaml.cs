﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Threading;
using FontAwesome.WPF;
using System.Linq;

namespace WPF_Client
{

    public class PlayerDisplay : Border
    {
        Response.playerInfo user;
        public PlayerDisplay(Response.playerInfo player, ref Page current)
        {
            const int LEN_LIMIT = 15;
            Grid display = new Grid();
            Button promote = new Button();
            Button demote = new Button();
            ImageAwesome promoteIcon = new ImageAwesome();
            ImageAwesome demoteIcon = new ImageAwesome();
            Label usernameDisplay = new Label();
            Label userRank = new Label();
            var page = (Page)current;

            user = player;

            Margin = new Thickness(0, 0, 0, 10);
            Background = Global.DefaultTextboxForegroundRed;
            CornerRadius = new CornerRadius(10);
            Height = 76.333;

            promote.Margin = new Thickness(450, 12.333, 0, 0);
            promote.Height = 48;
            promote.Width = 48;
            promote.BorderBrush = null;
            promote.Foreground = null;
            promote.Background = null;
            promoteIcon.Foreground = user.state == PlayerRank.MEMBER ? Brushes.White : Global.DefaultTextboxForegroundWhite;
            promoteIcon.Icon = FontAwesomeIcon.AngleUp;
            promoteIcon.Spin = false;
            promoteIcon.Height = 36;
            promoteIcon.Width = 36;
            promote.Content = promoteIcon;
            promote.Click += (source, eventArgs) =>
            {
                if (user.state == PlayerRank.MEMBER)
                {
                    if (!Global.ClientCommunicator.Promote(user.username) && Global.ClientCommunicator.HasErrorOccurred())
                    {
                        //reload
                        Global.ClientCommunicator.Reload(ref page);
                    }
                }
            };
            display.Children.Add(promote);

            demote.Margin = new Thickness(375, 12.333, 20, 0);
            demote.Height = 48;
            demote.Width = 48;
            demote.BorderBrush = null;
            demote.Foreground = null;
            demote.Background = null;
            demoteIcon.Foreground = user.state == PlayerRank.INVITED || user.state == PlayerRank.ADMIN ? Brushes.White : Global.DefaultTextboxForegroundWhite;
            demoteIcon.Icon = FontAwesomeIcon.AngleDown;
            demoteIcon.Spin = false;
            demoteIcon.Height = 36;
            demoteIcon.Width = 36;
            demote.Content = demoteIcon;
            demote.Click += (source, eventArgs) =>
            {
                if(user.state == PlayerRank.ADMIN || user.state == PlayerRank.INVITED)
                {
                    if ((!Global.ClientCommunicator.Demote(user.username) || !Global.ClientCommunicator.Uninvite(user.username)) && Global.ClientCommunicator.HasErrorOccurred())
                    {
                        //reload
                        Global.ClientCommunicator.Reload(ref page);
                    }
                }
                
            };
            display.Children.Add(demote);

            usernameDisplay.Content = player.username.Length > LEN_LIMIT ? player.username.Substring(0, LEN_LIMIT) + "..." : player.username;
            usernameDisplay.Margin = new Thickness(10, 12.333, 0, 0);
            usernameDisplay.FontFamily = new FontFamily("Impact");
            usernameDisplay.FontSize = 36;
            usernameDisplay.Background = null;
            usernameDisplay.BorderBrush = null;
            usernameDisplay.Foreground = Brushes.White;
            display.Children.Add(usernameDisplay);

            userRank.Content = player.state == PlayerRank.MEMBER ? "Member" : player.state == PlayerRank.CREATOR ? "Creator" : player.state == PlayerRank.ADMIN ? "Admin" : "Invited";
            userRank.Margin = new Thickness(275, 20, 125, 20);
            userRank.FontFamily = new FontFamily("MS Office Symbol Bold");
            userRank.HorizontalContentAlignment = HorizontalAlignment.Center;
            userRank.FontSize = 18;
            userRank.Background = null;
            userRank.BorderBrush = Brushes.White;
            userRank.BorderThickness = new Thickness(2);
            userRank.Foreground = Brushes.White;
            display.Children.Add(userRank);
            
            Child = display;
        }
    }
    public partial class RoomPage : Page
    {
        private Response.GetRoomInfo info;
        private Thread autoRefresher;
        private CancellationTokenSource cancellationTrigger = new CancellationTokenSource();
        //default value flags
        private bool questionCountDefault = true;
        private bool timeoutDefault = true;
        private bool usernameDefault = true;
        /*
         * C'tor
         */
        public RoomPage()
        {
            const int STRING_LIMIT = 18; //room name limit
            List<string> categories = Global.ClientCommunicator.GetCategories(); //get categories
            var page = (Page)this;

            InitializeComponent();

            info = Global.ClientCommunicator.GetRoomInfo();

            if (info != null && info.players != null)
            {
                RoomNameLabel.Content = info.room.name.Length > STRING_LIMIT ? info.room.name.Substring(0, STRING_LIMIT) + "..." : info.room.name; //set title

                if (!Global.IsSinglePlay)
                {
                    //add player display
                    foreach (Response.playerInfo player in info.players)
                    {
                        PlayerDisplayPanel.Items.Add(new PlayerDisplay(player, ref page));
                    }
                    PlayersPanel.Visibility = Visibility.Visible;
                    NoPlayersPanel.Visibility = Visibility.Hidden;

                    if (info.room.is_public)
                    {
                        UsernameField.Visibility = Visibility.Hidden;
                        InviteButton.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    PlayersPanel.Visibility = Visibility.Hidden;
                    NoPlayersPanel.Visibility = Visibility.Visible;
                }

                //add categories to combobox
                categories.Add("Any");
                foreach (string category in categories)
                {
                    CategoriesBox.Items.Add(category);
                }

                //add difficulty levels to combobox
                DiffLevelBox.Items.Add("easy");
                DiffLevelBox.Items.Add("medium");
                DiffLevelBox.Items.Add("hard");

                if (!Global.IsSinglePlay)
                {
                    autoRefresher = new Thread(PlayerDisplayAutoRefresher);
                    autoRefresher.Start(cancellationTrigger.Token);
                }
            }

        }

        /* Textbox focus start */
        private void QuestionCountField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref QuestionCountField, ref questionCountDefault);
        }

        private void QuestionCountField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref QuestionCountField, ref questionCountDefault, DefaultValues.QuestionsAmount.ToString());
        }

        private void TimeoutField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref TimeoutField, ref timeoutDefault);
        }

        private void TimeoutField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref TimeoutField, ref timeoutDefault, DefaultValues.Timeout.ToString());
        }

        private void UsernameField_Focus(object sender, RoutedEventArgs e)
        {
            if (usernameDefault)
            {
                UsernameField.Text = string.Empty;
                UsernameField.Foreground = Global.DefaultTextboxForegroundRed;
                usernameDefault = false;
            }
        }
        
        private void UsernameField_Unfocus(object sender, RoutedEventArgs e)
        {
            if (UsernameField.Text == "")
            {
                UsernameField.Text = "Type username here...";
                UsernameField.Foreground = new SolidColorBrush(Color.FromArgb(127, 255, 76, 94));
                usernameDefault = true;
            }
        }
        /* Textbox focus end */

        /*
         * get difficulty identifier.
         * input: difficulty string name.
         * output: id.
         */
        private byte GetDifficultyId(string diff)
        {
            return diff == "easy" ? (byte)0 : diff == "medium" ? (byte)1 : diff == "hard" ? (byte)2 : (byte)3;
        }

        /*
         * PlayButton click event.
         */
        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            ushort timeout = 0;
            ushort amount = 0;
            
            if(!Global.IsSinglePlay)
            {
                //wait thread to finish
                cancellationTrigger.Cancel();
            }
            if (Global.ClientCommunicator.StartGame(
                UInt16.TryParse(TimeoutField.Text, out timeout) ? timeout : DefaultValues.Timeout,
                UInt16.TryParse(QuestionCountField.Text, out amount) ? amount : DefaultValues.QuestionsAmount,
                GetDifficultyId(DiffLevelBox.SelectedItem == null ? "" : DiffLevelBox.SelectedItem.ToString()),
                CategoriesBox.SelectedItem == null ? "" : CategoriesBox.SelectedItem.ToString())) //start the game
            {
                this.NavigationService.Navigate(new GamePage(), UriKind.Relative); //go to the game page
            }
            else if(Global.ClientCommunicator.HasErrorOccurred())
            {
                var page = (Page)this;
                Global.ClientCommunicator.Reload(ref page);
            }
        }

        /*
         * CloseRoomButton click event
         */
        private void CloseRoomButton_Click(object sender, RoutedEventArgs e)
        {
            if (!Global.IsSinglePlay)
            {
                //wait thread to finish
                cancellationTrigger.Cancel();
            }
            if (Global.ClientCommunicator.CloseRoom()) //close room
            {
                this.NavigationService.Navigate(new MenuPage(false), UriKind.Relative); //go back to menu
            }
            else if (Global.ClientCommunicator.HasErrorOccurred())
            {
                var page = (Page)this;
                Global.ClientCommunicator.Reload(ref page);
            }
        }

        /*
        * player display auto refresher thread.
        * input: none.
        * output: none.
        */
        private void PlayerDisplayAutoRefresher(object obj)
        {
            Response.GetRoomInfo newInfo = null;
            CancellationToken token = (CancellationToken)obj;

            while (!token.IsCancellationRequested)
            {
                newInfo = Global.ClientCommunicator.GetRoomInfo();
                var page = (Page)this;
                if (newInfo == null || newInfo.players == null || Global.ClientCommunicator.HasErrorOccurred()) //check for errors
                {
                    //reload
                    Dispatcher.Invoke(() =>
                    {
                        cancellationTrigger.Cancel(); //therminate this thread
                        Global.ClientCommunicator.Reload(ref page);
                    });
                }
                else if(IsUpdateNeeded(ref newInfo)) //check if update is needed
                {
                    //update
                    info = newInfo;
                    Dispatcher.Invoke(() =>
                    {
                        PlayerDisplayPanel.Items.Clear();
                        foreach (Response.playerInfo player in info.players)
                        {
                            PlayerDisplayPanel.Items.Add(new PlayerDisplay(player, ref page));
                        }
                    });
                }
                Thread.Sleep(300); //sleep
            }
        }
        
        /*
         * checks if needed to update the player list and the room information.
         * input: new info.
         * output: flag.
         */
        private bool IsUpdateNeeded(ref Response.GetRoomInfo newInfo)
        {
            bool flag = false;
            int i = 0;

            if(newInfo != null || newInfo.players != null)
            {
                if (info.players.Count != newInfo.players.Count)
                {
                    flag = true;
                }
                else
                {
                    for (i = 0; i < info.players.Count; i++)
                    {
                        if (info.players.ToArray()[i].username != newInfo.players.ToArray()[i].username ||
                           info.players.ToArray()[i].state != newInfo.players.ToArray()[i].state)
                        {
                            flag = true;
                        }
                    }
                }
            }
            return flag;
        }

        /*
         * InviteButton click event.
         */
        private void InviteButton_Click(object sender, RoutedEventArgs e)
        {
            if(!usernameDefault) //if not default value in field
            {
                if (!Global.ClientCommunicator.Invite(UsernameField.Text) && Global.ClientCommunicator.HasErrorOccurred())
                {
                    //reload
                    var page = (Page)this;
                    Global.ClientCommunicator.Reload(ref page);
                }
            }
        }
    }
}