﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Threading;
using FontAwesome.WPF;
using System.Windows.Media.Animation;

namespace WPF_Client
{
    public partial class WaitPage : Page
    {
        private const string musicFileName = @"trivia_music.mp3"; //music file
        private static MediaPlayer mediaPlayer = new MediaPlayer(); //kahoot music player
        private CancellationTokenSource cancellationTrigger = new CancellationTokenSource();
        private MediaTimeline timeLine = new MediaTimeline(new Uri(System.IO.Path.GetFullPath(musicFileName))); //soundtrack time line
        private bool isPlaying = true; //is music playing flag
        private Thread checker; //can play checker thread
        /*
         * C'tor
         */
        public WaitPage()
        {
            InitializeComponent();
            //play music
            timeLine.RepeatBehavior = RepeatBehavior.Forever; //put on repeat
            mediaPlayer.Clock = timeLine.CreateClock(); 
            mediaPlayer.Clock.Controller.Begin(); //play
            //start the checker
            checker = new Thread(TryToPlay);
            checker.Start(cancellationTrigger.Token);
        }
        /*
         * GoBackButton click event
         */
        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {
            cancellationTrigger.Cancel();
            if (Global.ClientCommunicator.LeaveRoom()) //leave the room
            {
                mediaPlayer.Clock.Controller.Stop(); //stop music
                this.NavigationService.Navigate(new MenuPage(false), UriKind.Relative); //go back to menu
            }
            else if (Global.ClientCommunicator.HasErrorOccurred())
            {
                var page = (Page)this;
                Global.ClientCommunicator.Reload(ref page);
            }
        }

        /*
         * tries to connect to the game.
         * input: none.
         * output: none.
         */ 
        private void TryToPlay(object obj)
        {
            bool keep = true; //loop flag
            CancellationToken token = (CancellationToken)obj;

            while (!token.IsCancellationRequested && keep)
            {
                if (Global.ClientCommunicator.CanPlay()) //check if user can play
                {
                    keep = false;
                    //go to the game page
                    Dispatcher.Invoke(() => {
                        mediaPlayer.Clock.Controller.Stop(); //stop music
                        this.NavigationService.Navigate(new GamePage(), UriKind.Relative);
                    });
                }
                else if (Global.ClientCommunicator.HasErrorOccurred())
                {
                    keep = false;
                    var page = (Page)this;
                    Global.ClientCommunicator.Reload(ref page); //go back to menu
                    Dispatcher.Invoke(() =>
                    {
                        mediaPlayer.Clock.Controller.Stop(); //stop music
                    });
                }
                else if(Global.ClientCommunicator.GetRank() == PlayerRank.CREATOR && !Global.ClientCommunicator.HasErrorOccurred())
                {
                    keep = false;
                    //go to the room page
                    Dispatcher.Invoke(() => {
                        mediaPlayer.Clock.Controller.Stop(); //stop music
                        this.NavigationService.Navigate(new RoomPage(), UriKind.Relative);
                    });
                }
                else if(Global.ClientCommunicator.HasErrorOccurred())
                {
                    keep = false;
                    var page = (Page)this;
                    Global.ClientCommunicator.Reload(ref page); //go back to menu
                    Dispatcher.Invoke(() =>
                    {
                        mediaPlayer.Clock.Controller.Stop(); //stop music
                    });
                }
                Thread.Sleep(300); //sleep
            }
        }
        /*
         * music button click event
         */
        private void MusicButton_Click(object sender, RoutedEventArgs e)
        {

            ImageAwesome display = new ImageAwesome(); //create new image

            //set
            display.Spin = false;
            display.Foreground = Brushes.White;
            display.Height = 64;
            display.Width = 64;

            if (isPlaying)
            {
                display.Icon = FontAwesomeIcon.VolumeOff; //change icon
                MusicButton.Content = display;
                mediaPlayer.Clock.Controller.Stop(); //stop music
                isPlaying = false;
            }
            else
            {
                display.Icon = FontAwesomeIcon.VolumeUp; //change icon
                MusicButton.Content = display;
                mediaPlayer.Clock.Controller.Begin(); //play music
                isPlaying = true;
            }
        }
    }
}
