﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace WPF_Client
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            // close all active threads
            Environment.Exit(0);
        }
    }
}
