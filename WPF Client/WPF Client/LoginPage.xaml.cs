﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WPF_Client
{
    public partial class LoginPage : Page
    {
        private bool usernameDefault = true;
        private bool passwordDefault = true;

        public LoginPage()
        {
            InitializeComponent();

            Global.ClientCommunicator.Close();
            try
            {
                Global.ClientCommunicator.Open(); //try to connect
            }
            catch
            {
                var page = (Page)this;
                Global.ClientCommunicator.Reload(ref page);
            }
            ErrorGrid.Visibility = Visibility.Hidden;
        }

        /*
         * LoginButton click event. 
         */
        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if(!usernameDefault && !passwordDefault)
            {
                if (Global.ClientCommunicator.Login(UsernameField.Text, PasswordField.Text)) //login
                {
                    //get to the menu page
                    this.NavigationService.Navigate(new MenuPage(false), UriKind.Relative);
                }
                else if (Global.ClientCommunicator.HasErrorOccurred())
                {
                    //reload
                    var page = (Page)this;
                    Global.ClientCommunicator.Reload(ref page);
                }
                else
                {
                    ErrorGrid.Visibility = Visibility.Visible;
                }
            }
        }

        /* Textbox focus start */
        private void UsernameField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref UsernameField, ref usernameDefault);
        }

        private void UsernameField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref UsernameField, ref usernameDefault, "Username...");
        }

        private void PasswordField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref PasswordField, ref passwordDefault);
        }

        private void PasswordField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref PasswordField, ref passwordDefault, "Password...");
        }
        /* Textbox focus end */

        /*
         * SignupButton click event. 
         */
        private void SignupButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new SignupPage(), UriKind.Relative); //go to the signup page
        }

        /* 
         * OkayButton click event.
         */
        private void OkayButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorGrid.Visibility = Visibility.Hidden;
        }
    }
}
