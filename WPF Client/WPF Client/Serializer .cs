﻿using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace WPF_Client
{
    namespace Request
    {
        /*
         * Signup request data struct.
         */
        struct Signup
        {
            public string username { get; set; }
            public string password { get; set; }
            public string email { get; set; }
            public string address { get; set; }
            public string phone_number { get; set; }
            public string birth_date { get; set; }
        }

        /*
         * Login request data struct.
         */
        struct Login
        {
            public string username { get; set; }
            public string password { get; set; }
        }

        /*
         * Create room request data struct.
         */
        struct CreateRoom
        {
            public string name { get; set; }
            public ushort max_users { get; set; }
            public bool is_public { get; set; }
        };

        /*
         * Join room request data struct.
         */
        struct JoinRoom
        {
            public uint id { get; set; }
        };

        /*
         * Promote request data struct.
         */
        struct Promote
        {
            public string user { get; set; }
        };

        /*
         * Demote request data struct.
         */
        struct Demote
        {
            public string user { get; set; }
        };

        /*
         * Invite request data struct.
         */
        struct Invite
        {
            public string user { get; set; }
        };

        /*
         * Uninvite request data struct.
         */
        struct Uninvite
        {
            public string user { get; set; }
        };

        /*
         * Submit answer request data struct.
         */
        struct SubmitAnswer
        {
            public ushort answer_id { get; set; }
        };

        /*
         * Start game request data struct.
         */
        struct StartGame
        {
            public ushort time_per_question { get; set; }
            public ushort round_count { get; set; }
            public byte diff { get; set; }
            public string category { get; set; }
        };
    }
    
    class Serializer
    {
        /*
         * Serializes the request according to the protocol.
         * input: jason data string, message code.
         * output: serialized request.
         */
        private static List<byte> SerializeRequest(MessageCode code, string jsonData)
        {
            List<byte> request = new List<byte>();
            request.Add((byte)code);

            request.AddRange(BitConverter.EndianBitConverter.LittleEndian.GetBytes(jsonData.Length));
            request.AddRange(Encoding.ASCII.GetBytes(jsonData));

            return request;
        }

        /*
        * Serializes the signup request.
        * input: username, password, email, address, phone, birth date.
        * output: serialized request.
        */
        public static List<byte> SerializeSignupRequest(string username, string password, string email, string address, string phone, string birthDate)
        {
            Request.Signup signupData = new Request.Signup();

            signupData.username = username;
            signupData.password = password;
            signupData.email = email;
            signupData.address = address;
            signupData.phone_number = phone;
            signupData.birth_date = birthDate;

            return SerializeRequest(MessageCode.SIGNUP, JsonSerializer.Serialize(signupData));
        }

        /*
         * Serializes the login request.
         * input: username, password.
         * output: serialized request.
         */
        public static List<byte> SerializeLoginRequest(string username, string password)
        {
            Request.Login loginData = new Request.Login();

            loginData.password = password;
            loginData.username = username;

            return SerializeRequest(MessageCode.LOGIN, JsonSerializer.Serialize(loginData));
        }

        /*
        * Serializes the logout request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeLogoutRequest()
        {
            return SerializeRequest(MessageCode.LOGOUT, "");
        }

        /*
        * Serializes the get statistics request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeGetStatisticsRequest()
        {
            return SerializeRequest(MessageCode.GET_STATISTICS, "");
        }

        /*
        * Serializes the get rooms request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeGetRoomsRequest()
        {
            return SerializeRequest(MessageCode.GET_ROOMS, "");
        }

        /*
        * Serializes the get room info request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeGetRoomInfoRequest()
        {
            return SerializeRequest(MessageCode.GET_ROOM_INFO, "");
        }

        /*
        * Serializes the get room info request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeGetGameInfoRequest()
        {
            return SerializeRequest(MessageCode.GET_GAME_INFO, "");
        }

        /*
        * Serializes the signup request.
        * input: room name, max users, if public.
        * output: serialized request.
        */
        public static List<byte> SerializeCreateRoomRequest(string roomName, ushort maxUsers, bool isPublic)
        {
            Request.CreateRoom createRoomData = new Request.CreateRoom();

            createRoomData.name = roomName;
            createRoomData.max_users = maxUsers;
            createRoomData.is_public = isPublic;
            return SerializeRequest(MessageCode.CREATE_ROOM, JsonSerializer.Serialize(createRoomData));
        }

        /*
        * Serializes the join room request.
        * input: room id.
        * output: serialized request.
        */
        public static List<byte> SerializeJoinRoomRequest(uint roomid)
        {
            Request.JoinRoom joinRoomData = new Request.JoinRoom();

            joinRoomData.id = roomid;

            return SerializeRequest(MessageCode.JOIN_ROOM, JsonSerializer.Serialize(joinRoomData));
        }

        /*
        * Serializes the leave room info request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeLeaveRoomRequest()
        {
            return SerializeRequest(MessageCode.LEAVE_ROOM, "");
        }

        /*
        * Serializes the close room info request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeCloseRoomRequest()
        {
            return SerializeRequest(MessageCode.CLOSE_ROOM, "");
        }

        /*
        * Serializes the can play info request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeCanPlayRequest()
        {
            return SerializeRequest(MessageCode.CAN_PLAY, "");
        }

        /*
        * Serializes the promote request.
        * input: user.
        * output: serialized request.
        */
        public static List<byte> SerializePromoteRequest(string user)
        {
            Request.Promote promoteData = new Request.Promote();
            promoteData.user = user;

            return SerializeRequest(MessageCode.PROMOTE, JsonSerializer.Serialize(promoteData));
        }

        /*
        * Serializes the demote request.
        * input: user.
        * output: serialized request.
        */
        public static List<byte> SerializeDemoteRequest(string user)
        {
            Request.Demote demoteData = new Request.Demote();
            demoteData.user = user;

            return SerializeRequest(MessageCode.DEMOTE, JsonSerializer.Serialize(demoteData));
        }

        /*
        * Serializes the Invite request.
        * input: user.
        * output: serialized request.
        */
        public static List<byte> SerializeInviteRequest(string user)
        {
            Request.Invite inviteData = new Request.Invite();
            inviteData.user = user;

            return SerializeRequest(MessageCode.INVITE, JsonSerializer.Serialize(inviteData));
        }

        /*
        * Serializes the Uninvite request.
        * input: user.
        * output: serialized request.
        */
        public static List<byte> SerializeUninviteRequest(string user)
        {
            Request.Uninvite uninviteData = new Request.Uninvite();
            uninviteData.user = user;

            return SerializeRequest(MessageCode.UNINVITE, JsonSerializer.Serialize(uninviteData));
        }

        /*
        * Serializes the is admin request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeGetRankRequest()
        {
            return SerializeRequest(MessageCode.GET_RANK, "");
        }

        /*
        * Serializes the Get Question Categories request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeGetQuestionCategoriesRequest()
        {
            return SerializeRequest(MessageCode.GET_QUESTION_CATEGORIES, "");
        }

        /*
        * Serializes the start game request.
        * input: question timeout, round count, difficulty, optional category.
        * output: serialized request.
        */
        public static List<byte> SerializeStartGameRequest(ushort timePerQuestion, ushort roundCount, byte diff, string category = "")
        {
            Request.StartGame startGameData = new Request.StartGame();
            startGameData.time_per_question = timePerQuestion;
            startGameData.round_count = roundCount;
            startGameData.diff = diff;
            startGameData.category = category;

            return SerializeRequest(MessageCode.START_GAME, JsonSerializer.Serialize(startGameData));
        }

        /*
        * Serializes the get question request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeGetQuestionRequest()
        {
            return SerializeRequest(MessageCode.GET_QUESTION, "");
        }

        /*
        * Serializes the submit answer request.
        * input: question id, answer id.
        * output: serialized request.
        */
        public static List<byte> SerializeSubmitAnswerRequest(ushort answerid)
        {
            Request.SubmitAnswer submitAnswerData = new Request.SubmitAnswer();
            submitAnswerData.answer_id = answerid;

            return SerializeRequest(MessageCode.SUBMIT_ANSWER, JsonSerializer.Serialize(submitAnswerData));
        }

        /*
        * Serializes the get game results request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeGetGameResultsRequest()
        {
            return SerializeRequest(MessageCode.GET_GAME_RESULTS, "");
        }

        /*
        * Serializes the leave game request.
        * input: none.
        * output: serialized request.
        */
        public static List<byte> SerializeLeaveGameRequest()
        {
            return SerializeRequest(MessageCode.LEAVE_GAME, "");
        }
    }
}
