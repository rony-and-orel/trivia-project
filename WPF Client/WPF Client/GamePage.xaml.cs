﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Threading;

namespace WPF_Client
{
    public class AnswerButton : Button
    {
        private string answer;
        private ushort answerid;
        /*
         * C'tor
         */
        public AnswerButton(string possibleAnswer, ushort answerId) : base()
        {
            Style borderStyle = new Style(typeof(Border)); //border style
            TextBlock content = new TextBlock(); //content text block
            //set fields
            answerid = answerId;
            answer = possibleAnswer;
            //set content
            content.Text = answer;
            content.TextWrapping = TextWrapping.Wrap;
            Content = content;
            //set colors
            BorderBrush = null;
            Foreground = Global.DefaultTextboxForegroundRed; //foreground color
            Background = Brushes.White;
            //set font
            FontStyle = FontStyles.Normal;
            FontFamily = new FontFamily("Impact");
            FontSize = 24;
            //set border style
            borderStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(5)));
            Resources.Add(typeof(Border), borderStyle);
        }
        /*
         * getter for answer.
         */
        public string GetAnswer()
        {
            return answer;
        }
        /*
         * getter for answer id.
         */
        public ushort GetAnswerId()
        {
            return answerid;
        }
        public void Correct()
        {
            Foreground = Brushes.White;
            Background = Brushes.LightGreen;
        }
        public void Wrong()
        {
            Foreground = Brushes.White;
            Background = Brushes.PaleVioletRed;
        }
    }
    public partial class GamePage : Page
    {
        private Response.GetGameInfo info; //game info
        private CancellationTokenSource sechdulerCancellationTrigger = new CancellationTokenSource();
        private CancellationTokenSource timerCancellationTrigger = new CancellationTokenSource();
        private static bool isSubmitted = false; //is answer submitted flag
        private List<AnswerButton> answers; //answer buttons
        private Thread questioner; //questioner thread
        
        public GamePage()
        {
            InitializeComponent();
            //set
            info = Global.ClientCommunicator.GetGameInfo();
            answers = new List<AnswerButton>();
            //init new thread
            questioner = new Thread(()=>scheduler(info.question_amount, sechdulerCancellationTrigger.Token));
            questioner.Start();

            WaitingMessage.Visibility = Visibility.Hidden;
        }

        /*
         * resets the timer bar.
         * input: none.
         * output: none.
         */
        private void resetTimeoutBar(ushort startTime)
        {
            TimeoutBar.Value = (startTime / 10);
            TimeoutBar.Maximum = (info.timeout - 1) * 100;
            TimeoutBar.Minimum = 0;
        }

        /*
         * displays the next question.
         * input: none.
         * output: is succeeded flag
         */
        private void setNextQuestion(Response.GetQuestion question)
        {
            AnswerButton btn = null;
            //set
            isSubmitted = false;
            QuestionText.Text = question.question;
            //clear
            AnswerPanel.Children.Clear();
            answers.Clear();
            //create answer buttons
            foreach (string answer in question.answers)
            {
                btn = new AnswerButton(answer, (ushort)Array.IndexOf(question.answers.ToArray(), answer)); //create new display button
                //set event click
                btn.Click += (source, eventArgs) =>
                {
                    AnswerButton button = source as AnswerButton;
                    Response.SubmitAnswer res = isSubmitted ? null : Global.ClientCommunicator.SubmitAnswer(button.GetAnswerId()); //submit
                    if (Global.ClientCommunicator.HasErrorOccurred())
                    {
                        sechdulerCancellationTrigger.Cancel();
                        timerCancellationTrigger.Cancel();
                        var page = (Page)this;
                        Global.ClientCommunicator.Reload(ref page);
                    }
                    else if (!isSubmitted && res.status == StatusCode.STATUS_SUCCESS) //check
                    {
                        //sign as correct
                        button.Correct();
                        isSubmitted = true;
                    }
                    else if (!isSubmitted && res != null)
                    {
                        //sign as wrong
                        button.Wrong();
                        isSubmitted = true;
                        answers.ToArray()[res.correct_answer_id].Correct(); //sign the correct answer
                    }
                    
                };
                answers.Add(btn); //add to list
                AnswerPanel.Children.Add(btn); //add to panel display
            }
        }

        /*
         * LeaveButton click event.
         */
        private void LeaveButton_Click(object sender, RoutedEventArgs e)
        {
            sechdulerCancellationTrigger.Cancel();
            if (Global.ClientCommunicator.LeaveGame()) //leave game
            {
                this.NavigationService.Navigate(new MenuPage(false), UriKind.Relative); //go back to menu page
            }
            else if (Global.ClientCommunicator.HasErrorOccurred())
            {
                var page = (Page)this;
                Global.ClientCommunicator.Reload(ref page);
            }
        }

        /*
         * LogoutButton click event.
         */
        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            sechdulerCancellationTrigger.Cancel();
            if (Global.ClientCommunicator.LeaveGame() && Global.ClientCommunicator.Logout())
            {
                this.NavigationService.Navigate(new LoginPage(), UriKind.Relative); //go back to login page
            }
            else if (Global.ClientCommunicator.HasErrorOccurred())
            {
                var page = (Page)this;
                Global.ClientCommunicator.Reload(ref page);
            }
        }

        /*
         * question schedular thread.
         * input: number of rounds.
         * output: none.
         */
        private void scheduler(uint rounds, object obj)
        {
            uint questionCounter = 1; //counter
            Thread timer = null; //timer thread
            bool flag = true; //next question flag
            bool keep = true; //keep getting questions
            Response.GetQuestion question = null; //get question
            Response.GetGameResults results = null; //game results
            CancellationToken token = (CancellationToken)obj;

            while (keep && !token.IsCancellationRequested) //for each round
            {
                if(flag) //if needed next question
                {
                    do
                    {
                        question = Global.ClientCommunicator.GetQuestion(); //get question
                        if (question != null && question.status == StatusCode.STATUS_SUCCESS) //check if can set
                        {
                            Dispatcher.Invoke(() =>
                            {
                                //set question count
                                questionCounter = question.id;
                                //set next question
                                setNextQuestion(question);
                                //set next question index
                                QuestionCount.Content = "Question " + questionCounter.ToString() + " out of " + rounds.ToString();
                                resetTimeoutBar(question.time);
                                WaitingMessage.Visibility = Visibility.Hidden;
                            });
                            timerCancellationTrigger = new CancellationTokenSource(); //set new cancellation trigger
                            timer = new Thread(questionTimer); //set timer thread
                            timer.Start(timerCancellationTrigger.Token);
                        }
                        else if (Global.ClientCommunicator.HasErrorOccurred())
                        {
                            //reload connection
                            var page = (Page)this;
                            Dispatcher.Invoke(() =>
                            {
                                sechdulerCancellationTrigger.Cancel();
                                Global.ClientCommunicator.Reload(ref page);
                            });
                        }
                        else
                        {
                            Thread.Sleep(100); //wait
                        }
                    } while ((question == null || question.status != StatusCode.STATUS_SUCCESS) && !token.IsCancellationRequested); //check if needed to try again
                    flag = false;
                }
                if((isSubmitted || !timer.IsAlive) && !token.IsCancellationRequested) //if the answer has already submitted or the time is out
                {
                    if(questionCounter == rounds)
                    {
                        keep = false;
                    }
                    flag = true;
                    if(isSubmitted)
                    {
                        Thread.Sleep(500); // delay to receive submit result
                    }
                    timerCancellationTrigger.Cancel();
                    Dispatcher.Invoke(() =>
                    {
                        WaitingMessage.Visibility = Visibility.Visible;
                    });
                }
            }
            if (!token.IsCancellationRequested)
            {
                //if game was finished
                do
                {
                    results = Global.ClientCommunicator.GetResults(); //get game results
                    Thread.Sleep(300); // wait
                } while (results.status != StatusCode.STATUS_SUCCESS); //try again if can't get
                
                Dispatcher.Invoke(() =>
                {
                    this.NavigationService.Navigate(new EndGamePage(results, info), UriKind.Relative); //go to end game page
                });
            }
        }

        /*
         * question timer thread.
         * input: none.
         * output: none.
         */
        private void questionTimer(object obj)
        {
            double current = 0; //current time
            double max = 0; //maximum time
            CancellationToken token = (CancellationToken)obj;

            Dispatcher.Invoke(() =>
            {
                max = TimeoutBar.Maximum;
            });

            do
            {
                Dispatcher.Invoke(() => {
                    TimeoutBar.Value++; //progress
                    current = TimeoutBar.Value; //set
                });
                Thread.Sleep(10); //wait
            } while (current < max && !token.IsCancellationRequested);
        }
    }
}
