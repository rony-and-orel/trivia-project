﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace WPF_Client
{
    /*
     * client configuration data struct.
     * used to extract values from the JSON config file.
     */
    struct configData
    {
        public string server_ip { get; set; }
        public int server_port { get; set; }
    }
    class Config
    {
        private const string cofigFileName = @"config.json"; //config file name

        /*
         * get server ip address.
         * input: none.
         * output: ip address.
         */
        public static string GetServerIP()
        {
            configData data = JsonSerializer.Deserialize<configData>(System.IO.File.ReadAllText(System.IO.Path.GetFullPath(cofigFileName)));

            return data.server_ip;
        }

        /*
         * get server port number.
         * input: none.
         * output: port number.
         */
        public static int GetServerPort()
        {
            configData data = JsonSerializer.Deserialize<configData>(System.IO.File.ReadAllText(System.IO.Path.GetFullPath(cofigFileName)));

            return data.server_port;
        }

        /*
        * get server ip address.
        * input: ip address.
        * output: none.
        */
        public static void SetServerIP(string ip)
        {
            configData data = JsonSerializer.Deserialize<configData>(System.IO.File.ReadAllText(System.IO.Path.GetFullPath(cofigFileName)));

            data.server_ip = ip;

            System.IO.File.WriteAllText(System.IO.Path.GetFullPath(cofigFileName), JsonSerializer.Serialize(data));
        }

        /*
        * set server port number.
        * input: port number.
        * output: none.
        */
        public static void SetServerPort(int port)
        {
            configData data = JsonSerializer.Deserialize<configData>(System.IO.File.ReadAllText(System.IO.Path.GetFullPath(cofigFileName)));

            data.server_port = port;

            System.IO.File.WriteAllText(System.IO.Path.GetFullPath(cofigFileName), JsonSerializer.Serialize(data));
        }
    }
}
