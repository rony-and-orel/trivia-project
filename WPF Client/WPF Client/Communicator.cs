﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Threading;
using System.Threading;

namespace WPF_Client
{
    public class Communicator
    {
        /*
         * logged user data class.
         */
        private class ConnectedUser
        {
            public string username { get; set; }
            public string password { get; set; }
        }

        private ConnectedUser user = null; //connected user instance
        private System.Net.Sockets.TcpClient connection = null; // tcp connection handler
        private bool errorFlag = true; //communicator error flag
        private Mutex commLock = new Mutex();

        /*
         * C'tor
         */
        public Communicator()
        {
        }

        /*
         * opens the socket connection.
         * input: none.
         * output: none.
         */
        public void Open()
        {
            //create new socket and connect
            if (connection == null)
            {
                connection = new System.Net.Sockets.TcpClient();
                connection.Connect(Config.GetServerIP(), Config.GetServerPort());
            }
        }

        /*
        * closes the socket connection.
        * input: none.
        * output: none.
        */
        public void Close()
        {
            if (connection != null)
            {
                connection.Close();
                connection = null;
            }
        }

        /*
         * manage session between the client and the server.
         * input: client message.
         * output: server response.
         */
        private byte[] Session(List<byte> message)
        {
            byte[] header = new byte[(int)Sizes.HEADER_SIZE]; //message header
            byte[] data = null; //message data

            errorFlag = false; //reset flag

            commLock.WaitOne(); //lock
            connection.Client.Send(message.ToArray()); //send login request
            commLock.ReleaseMutex(); //unlock
            connection.Client.Receive(header, (int)Sizes.HEADER_SIZE, SocketFlags.None); //get header
            
            data = new byte[Deserializer.GetDocumentLength(header)]; //set data message length
            connection.Client.Receive(data, (int)Deserializer.GetDocumentLength(header), SocketFlags.None); //get data

            return data;
        }

        /*
        * reloads client connection.
        * input: none.
        * output: success flag.
        */
        public void Reload(ref Page current)
        {
            var page = current;
            //reload connection
            Close();
            try
            {
                Open(); //open new connection
                if (user != null)
                {
                    //try to login again
                    Session(Serializer.SerializeLoginRequest(user.username, user.password));
                    current.Dispatcher.Invoke(() =>
                    {
                        page.NavigationService.Navigate(new MenuPage(true), UriKind.Relative);
                    });
                }
                else
                {
                    Close(); //close
                    current.Dispatcher.Invoke(() =>
                    {
                        page.NavigationService.Navigate(new LoginPage(), UriKind.Relative); //open again in login
                    });
                }
                errorFlag = false; //reset flag
            }
            catch
            {
                //shutdown application
                MessageBox.Show("Unable to connect to the server.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(0);
            }
        }

        /*
        * signs in to the trivia game server.
        * input: username, password.
        * output: success flag.
        */
        public bool Login(string username, string password)
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeLoginRequest(username, password));
                success = Deserializer.DeserializeLoginResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }

            if (success)
            {
                user = new ConnectedUser { username = username, password = password }; //save user instance
            }
            return success;
        }

        /*
        * registers a new account in the trivia game server.
        * input: username, password, email, address, phone, birth date.
        * output: success flag.
        */
        public bool Signup(string username, string password, string email, string address, string phone, string birthDate)
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeSignupRequest(username, password, email, address, phone, birthDate));
                success = Deserializer.DeserializeSignupResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }

            if (success)
            {
                user = new ConnectedUser { username = username, password = password }; //save user instance
            }
            return success;
        }

        /*
        * creating a new game room.
        * input: room name, max users, if public.
        * output: success flag.
        */
        public bool CreateRoom(string roomName, ushort maxUsers, bool isPublic)
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeCreateRoomRequest(roomName, maxUsers, isPublic));
                success = Deserializer.DeserializeCreateRoomResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return success;
        }

        /*
        * joins user to a game room.
        * input: room id.
        * output: success flag.
        */
        public bool JoinRoom(uint roomid)
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeJoinRoomRequest(roomid));
                success = Deserializer.DeserializeJoinRoomResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return success;
        }

        /*
        * requesting for the list of players in a specific room.
        * input: none.
        * output: room info response.
        */
        public Response.GetRoomInfo GetRoomInfo()
        {
            Response.GetRoomInfo info = null; //players list
            byte[] response = null; //response message

            try
            {
                response = Session(Serializer.SerializeGetRoomInfoRequest());
                info = Deserializer.DeserializeGetRoomInfoResponse(response); //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return info;
        }

        /*
        * requesting for the list of rooms.
        * input: none.
        * output: room data list.
        */
        public Response.GetRooms GetRooms()
        {
            Response.GetRooms rooms = null; //rooms list
            byte[] response = null; //response message

            try
            {
                response = Session(Serializer.SerializeGetRoomsRequest());
                rooms = Deserializer.DeserializeGetRoomsResponse(response); //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return rooms;
        }

        /*
        * loging out of the trivia game server.
        * input: none.
        * output: success flag.
        */
        public bool Logout()
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeLogoutRequest());
                success = Deserializer.DeserializeLogoutResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }

            if (success)
            {
                user = null; //remove user instance
            }
            return success;
        }

        /*
        * requesting for user statistics.
        * input: none.
        * output: statistics.
        */
        public Response.GetStatistics GetStatistics()
        {
            Response.GetStatistics statistics = null; //statistics
            byte[] response = null; //response message

            try
            {
                response = Session(Serializer.SerializeGetStatisticsRequest());
                statistics = Deserializer.DeserializeGetStatisticsResponse(response); //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return statistics;
        }

        /*
        * requesting for question categories.
        * input: none.
        * output: categories.
        */
        public List<string> GetCategories()
        {
            List<string> categories = null; //players list
            byte[] response = null; //response message

            try
            {
                response = Session(Serializer.SerializeGetQuestionCategoriesRequest());
                categories = Deserializer.DeserializeGetQuestionCategoriesResponse(response).categories; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return categories;
        }

        /*
        * starts the trivia game in the current room.
        * input: room id.
        * output: success flag.
        */
        public bool StartGame(ushort timePerQuestion, ushort roundCount, byte diff, string category = "")
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeStartGameRequest(timePerQuestion, roundCount, diff, category));
                success = Deserializer.DeserializeJoinRoomResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return success;
        }

        /*
        * leaves the current room.
        * input: room id.
        * output: success flag.
        */
        public bool LeaveRoom()
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeLeaveRoomRequest());
                success = Deserializer.DeserializeLeaveRoomResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return success;
        }

        /*
        * leaves the current game.
        * input: room id.
        * output: success flag.
        */
        public bool LeaveGame()
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeLeaveGameRequest());
                success = Deserializer.DeserializeLeaveGameResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return success;
        }

        /*
        * closes the current room.
        * input: room id.
        * output: success flag.
        */
        public bool CloseRoom()
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeCloseRoomRequest());
                success = Deserializer.DeserializeCloseRoomResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return success;
        }

        public bool Promote(string username)
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializePromoteRequest(username));
                success = Deserializer.DeserializeJoinPromoteResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return success;
        }

        public bool Demote(string username)
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeDemoteRequest(username));
                success = Deserializer.DeserializeJoinDemoteResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return success;
        }

        public bool Invite(string username)
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeInviteRequest(username));
                success = Deserializer.DeserializeInviteResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return success;
        }

        public bool Uninvite(string username)
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeUninviteRequest(username));
                success = Deserializer.DeserializeUninviteResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return success;
        }

        /*
        * closes the current room.
        * input: room id.
        * output: success flag.
        */
        public bool CanPlay()
        {
            byte[] response = null; //response message
            bool success = false; //success flag

            try
            {
                response = Session(Serializer.SerializeCanPlayRequest());
                success = Deserializer.DeserializeCanPlayResponse(response).status == StatusCode.STATUS_SUCCESS; //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return success;
        }

        /*
        * get user rank in current room.
        * input: room id.
        * output: success flag.
        */
        public PlayerRank GetRank()
        {
            byte[] response = null; //response message
            PlayerRank rank = PlayerRank.NONE; //rank

            try
            {
                response = Session(Serializer.SerializeGetRankRequest());
                rank = Deserializer.DeserializeGetRankResponse(response).rank; //deserialize
                if(Deserializer.DeserializeGetRankResponse(response).status == StatusCode.STATUS_FAIL)
                {
                    errorFlag = true; //set error
                }
            }
            catch
            {
                errorFlag = true; //set error
            }
            return rank;
        }

        /*
        * requesting for game info.
        * input: none.
        * output: info.
        */
        public Response.GetGameInfo GetGameInfo()
        {
            Response.GetGameInfo info = null; //game info
            byte[] response = null; //response message

            try
            {
                response = Session(Serializer.SerializeGetGameInfoRequest());
                info = Deserializer.DeserializeGetGameInfoResponse(response); //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return info;
        }

        /*
       * requesting for the next question.
       * input: none.
       * output: info.
       */
        public Response.GetQuestion GetQuestion()
        {
            Response.GetQuestion question = null; //question
            byte[] response = null; //response message

            try
            {
                response = Session(Serializer.SerializeGetQuestionRequest());
                question = Deserializer.DeserializeGetQuestionResponse(response); //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return question;
        }

        /*
        * submits the answer.
        * input: answer id.
        * output: submit answer data.
        */
        public Response.SubmitAnswer SubmitAnswer(ushort answerid)
        {
            byte[] response = null; //response message
            Response.SubmitAnswer answer = null; //question

            try
            {
                response = Session(Serializer.SerializeSubmitAnswerRequest(answerid));
                answer = Deserializer.DeserializeSubmitAnswerResponse(response); //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return answer;
        }

        /*
        * get game results.
        * input: none.
        * output: player results list.
        */
        public Response.GetGameResults GetResults()
        {
            byte[] response = null; //response message
            Response.GetGameResults results = null; //result list

            try
            {
                response = Session(Serializer.SerializeGetGameResultsRequest());
                results = Deserializer.DeserializeGetGameResultsResponse(response); //deserialize
            }
            catch
            {
                errorFlag = true; //set error
            }
            return results;
        }

        /*
         * getter for the comunicator error flag.
         * input: none.
         * output: error flag.
         */
        public bool HasErrorOccurred()
        {
            return errorFlag;
        }

        /*
         * returns the current connected username.
         * input: none.
         * output: username.
         */
        public string GetUserName()
        {
            return user != null ? user.username : "";
        }
    }
}
