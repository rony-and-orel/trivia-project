﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Threading;

namespace WPF_Client
{
    /*
     * room display button class for the join panel in the menu page.
     */
    public class RoomDisplayButton : Button
    {
        private RoomData room; //room data
        /*
         * C'tor
         */
        public RoomDisplayButton(RoomData data) : base()
        {
            const int CUT_AMOUNT = 6; //cut amount of room name
            Style style = new Style(typeof(Border)); //style

            room = data; //set data
            Content = room.name.Length > CUT_AMOUNT ? room.name.Substring(0, CUT_AMOUNT) + "..." : room.name; //set content

            //set colors
            BorderBrush = null;
            Foreground = Global.DefaultTextboxForegroundRed;
            Background = Brushes.White;
            //set font
            FontStyle = FontStyles.Normal;
            FontFamily = new FontFamily("Impact");
            FontSize = 20;
            //set style
            style.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(5)));
            this.Resources.Add(typeof(Border), style);
        }
        /*
         * getter for room data.
         * input: none.
         * output: room data.
         */
        public RoomData GetRoomData()
        {
            return room;
        }
    }
    public partial class MenuPage : Page
    {
        private bool isSideBarCollapsed = false; //is side bar collapsed flag
        private Response.GetRooms roomlist = null; //updated room list
        private Thread autoRefresher; //auto refresher for the room diplay
        private CancellationTokenSource cancellationTrigger = new CancellationTokenSource();
        //default text value flags
        private bool roomNameDefault = true;
        private bool userCountDefault = true;
        private bool ipDefault = true;
        private bool portDefault = true;
        /*
         * C'tor
         */
        public MenuPage(bool isError)
        {
            InitializeComponent();

            welcomeUser(); //initiate side bar

            //set visibility
            CreateRoomPanel.Visibility = Visibility.Hidden;
            JoinRoomPanel.Visibility = Visibility.Hidden;
            SettingsPanel.Visibility = Visibility.Hidden;
            ErrorGrid.Visibility = Visibility.Hidden;

            if (isError)
            {
                ErrorGrid.Visibility = Visibility.Visible;
            }

            autoRefresher = new Thread(RoomDisplayAutoRefresher);
            autoRefresher.Start(cancellationTrigger.Token);

            Global.IsSinglePlay = false; //reset "is single play" flag
        }

        /*
         * initiates and opens the side bar display.
         * input: none.
         * output: none.
         */
        private void welcomeUser()
        {
            WelcomeUserLabel.Content = "Hello, " + Global.ClientCommunicator.GetUserName(); //set side bar username display

            Response.GetStatistics statistics = Global.ClientCommunicator.GetStatistics(); //get statistics
            if (statistics != null)
            {
                //init statistics panel values
                NumberOfGamesLabel.Content = statistics.games_played;
                NumberOfStrikesLabel.Content = statistics.correct_answers;
                NumberOfSecondsLabel.Content = statistics.avg_time_per_answer.Substring(0, 3);
            }
            else if (Global.ClientCommunicator.HasErrorOccurred())
            {
                var page = (Page)this;
                Global.ClientCommunicator.Reload(ref page);
            }
        }

        /*
         * CreateRoomButton click event.
         */
        private void CreateRoomButton_Click(object sender, RoutedEventArgs e)
        {
            CreateRoomPanel.Visibility = Visibility.Visible;
            JoinRoomPanel.Visibility = Visibility.Hidden;
            SettingsPanel.Visibility = Visibility.Hidden;
        }

        /*
        * JoinRoomButton click event.
        */
        private void JoinRoomButton_Click(object sender, RoutedEventArgs e)
        {
            //set panels visibility
            CreateRoomPanel.Visibility = Visibility.Hidden;
            SettingsPanel.Visibility = Visibility.Hidden;
            NoRoomsLabel.Visibility = Visibility.Hidden;
            JoinRoomPanel.Visibility = Visibility.Visible;
        }

        /*
        * SinglePlayButton click event.
        */
        private void SinglePlayButton_Click(object sender, RoutedEventArgs e)
        {
            //set panels visibility
            CreateRoomPanel.Visibility = Visibility.Hidden;
            JoinRoomPanel.Visibility = Visibility.Hidden;
            SettingsPanel.Visibility = Visibility.Hidden;

            cancellationTrigger.Cancel();
            if (Global.ClientCommunicator.CreateRoom("My Single Play", 1, false)) //create the private room
            {
                Global.IsSinglePlay = true; //set as single play
                this.NavigationService.Navigate(new RoomPage(), UriKind.Relative); //go to room page
            }
            else if (Global.ClientCommunicator.HasErrorOccurred())
            {
                var page = (Page)this;
                Global.ClientCommunicator.Reload(ref page);
            }
        }

        /*
        * LogoutButton click event.
        */
        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            cancellationTrigger.Cancel();
            if (!Global.ClientCommunicator.Logout())
            {
                if (Global.ClientCommunicator.HasErrorOccurred())
                {
                    var page = (Page)this;
                    Global.ClientCommunicator.Reload(ref page);
                }
            }
            this.NavigationService.Navigate(new LoginPage(), UriKind.Relative);
        }

        /*
        * SettingsButton click event.
        */
        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            //set panels visibility
            SettingsPanel.Visibility = Visibility.Visible;
            CreateRoomPanel.Visibility = Visibility.Hidden;
            JoinRoomPanel.Visibility = Visibility.Hidden;

            //get config data
            IPLabel.Content = Config.GetServerIP();
            PortLabel.Content = Config.GetServerPort().ToString();

            //set elements visibility
            IPLabel.Visibility = Visibility.Visible;
            PortLabel.Visibility = Visibility.Visible;

            IPField.Visibility = Visibility.Hidden;
            PortField.Visibility = Visibility.Hidden;

            SaveChangesButton.Visibility = Visibility.Hidden;
            EditButton.Visibility = Visibility.Visible;
            NoteChangesLabel.Visibility = Visibility.Hidden;
            
            //enable edit button
            EditButton.IsEnabled = true;
            EditButton.Opacity = 1;
        }

        /*
        * ScrollPanelButton click event.
        */
        private void ScrollPanelButton_Click(object sender, RoutedEventArgs e)
        {
            const int SCROLL_AMOUNT = 322; //amount of distance to scroll
            if (!isSideBarCollapsed)
            {
                SideBarMenu.Width -= SCROLL_AMOUNT; //reduce side bar menu width
                //hide
                StatisticsPanel.Visibility = Visibility.Hidden;
                ninjaUserPic.Visibility = Visibility.Hidden;
                WelcomeUserLabel.Visibility = Visibility.Hidden;
                isSideBarCollapsed = true;

                //change panels position
                CreateRoomPanel.Margin = new Thickness(CreateRoomPanel.Margin.Left - SCROLL_AMOUNT, 0, 0, 0);
                JoinRoomPanel.Margin = new Thickness(JoinRoomPanel.Margin.Left - SCROLL_AMOUNT, 0, 0, 0);
                SettingsPanel.Margin = new Thickness(SettingsPanel.Margin.Left - SCROLL_AMOUNT, 0, 0, 0);
            }
            else
            {
                SideBarMenu.Width += SCROLL_AMOUNT; //increase side bar menu width
                //show
                StatisticsPanel.Visibility = Visibility.Visible;
                ninjaUserPic.Visibility = Visibility.Visible;
                WelcomeUserLabel.Visibility = Visibility.Visible;
                isSideBarCollapsed = false;

                //change panels position
                CreateRoomPanel.Margin = new Thickness(CreateRoomPanel.Margin.Left + SCROLL_AMOUNT, 0, 0, 0);
                JoinRoomPanel.Margin = new Thickness(JoinRoomPanel.Margin.Left + SCROLL_AMOUNT, 0, 0, 0);
                SettingsPanel.Margin = new Thickness(SettingsPanel.Margin.Left + SCROLL_AMOUNT, 0, 0, 0);
            }
        }

        /* Textbox focus start */
        private void RoomNameField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref RoomNameField, ref roomNameDefault);
        }

        private void RoomNameField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref RoomNameField, ref roomNameDefault, "My Awesome Room...");
        }

        private void UserCountField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref UserCountField, ref userCountDefault);
        }

        private void UserCountField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref UserCountField, ref userCountDefault, DefaultValues.UsersAmount.ToString());
        }

        private void IPField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref IPField, ref ipDefault);
        }

        private void IPField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref IPField, ref ipDefault, Config.GetServerIP());
        }

        private void PortField_Focus(object sender, RoutedEventArgs e)
        {
            Global.TextboxFocus(ref PortField, ref portDefault);
        }
        
        private void PortField_Unfocus(object sender, RoutedEventArgs e)
        {
            Global.TextboxUnfocus(ref PortField, ref portDefault, Config.GetServerPort().ToString());
        }
        /* Textbox focus end */

        /*
        * CreateButton click event.
        */
        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            ushort amount = 0;
            cancellationTrigger.Cancel();
            if (Global.ClientCommunicator.CreateRoom(RoomNameField.Text,
                                       UInt16.TryParse(UserCountField.Text, out amount) ? amount : DefaultValues.UsersAmount,
                                       isPrivateCheckbox.IsChecked == true ? false : true)) //create rooms
            {
                this.NavigationService.Navigate(new RoomPage(), UriKind.Relative); //go to room page
            }
            else if (Global.ClientCommunicator.HasErrorOccurred())
            {
                var page = (Page)this;
                Global.ClientCommunicator.Reload(ref page);
            }
        }

        /*
        * EditButton click event.
        */
        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            //get config data
            IPField.Text = Config.GetServerIP();
            PortField.Text = Config.GetServerPort().ToString();

            //hide
            IPLabel.Visibility = Visibility.Hidden;
            PortLabel.Visibility = Visibility.Hidden;
            //show
            IPField.Visibility = Visibility.Visible;
            PortField.Visibility = Visibility.Visible;
            SaveChangesButton.Visibility = Visibility.Visible;
            NoteChangesLabel.Visibility = Visibility.Visible;

            //disable edit button
            EditButton.IsEnabled = false;
            EditButton.Opacity = 0.75;
        }

        /*
        * SaveChangesButton click event.
        */
        private void SaveChangesButtonClick(object sender, RoutedEventArgs e)
        {
            //set the new data
            Config.SetServerIP(IPField.Text);
            Config.SetServerPort(Int32.Parse(PortField.Text));

            //load the new data
            IPLabel.Content = Config.GetServerIP();
            PortLabel.Content = Config.GetServerPort().ToString();

            //show
            IPLabel.Visibility = Visibility.Visible;
            PortLabel.Visibility = Visibility.Visible;
            EditButton.Visibility = Visibility.Visible;

            //hide
            IPField.Visibility = Visibility.Hidden;
            PortField.Visibility = Visibility.Hidden;
            SaveChangesButton.Visibility = Visibility.Hidden;
            NoteChangesLabel.Visibility = Visibility.Hidden;

            //enable edit button
            EditButton.IsEnabled = true;
            EditButton.Opacity = 1;
        }

        /*
         * checks if needed to update the room list with the new information.
         * input: new info.
         * output: flag.
         */
        private bool IsUpdateNeeded(Response.GetRooms newList)
        {
            bool flag = false;
            int i = 0;

            if (newList != null || newList.rooms != null)
            {
                if (roomlist.rooms.Count != newList.rooms.Count)
                {
                    flag = true;
                }
                else
                {
                    for (i = 0; i < roomlist.rooms.Count; i++)
                    {
                        if (roomlist.rooms.ToArray()[i].id != newList.rooms.ToArray()[i].id)
                        {
                            flag = true;
                        }
                    }
                }
            }
            return flag;
        }

        /*
         * sets a new room display in join room panel.
         * input: new room list.
         * output: none.
         */
        private void SetRoomDisplay(ref Response.GetRooms newRoomList)
        {
            RoomDisplayButton btn = null;
            var page = (Page)this;

            if (newRoomList != null && newRoomList.rooms != null && newRoomList.rooms.Any()) //if there are rooms
            {
                if (roomlist == null || roomlist.rooms == null || IsUpdateNeeded(newRoomList)) // if list should be updated
                {
                    RoomsDisplay.Items.Clear();
                    foreach (RoomData room in newRoomList.rooms)
                    {
                        if (room != null)
                        {
                            btn = new RoomDisplayButton(room); //create new display button
                            //set event click
                            btn.Click += (source, eventArgs) =>
                            {
                                RoomDisplayButton button = source as RoomDisplayButton;
                                cancellationTrigger.Cancel();
                                if (Global.ClientCommunicator.JoinRoom((uint)button.GetRoomData().id)) //join to the selected room
                                {
                                    if (Global.ClientCommunicator.GetRank() == PlayerRank.CREATOR) //check user rank
                                    {
                                        this.NavigationService.Navigate(new RoomPage(), UriKind.Relative); //go to room page
                                    }
                                    else if (Global.ClientCommunicator.HasErrorOccurred())
                                    {
                                        //reload
                                        Global.ClientCommunicator.Reload(ref page);
                                    }
                                    else
                                    {
                                        this.NavigationService.Navigate(new WaitPage(), UriKind.Relative); //go to wait page
                                    }
                                }
                                else if (Global.ClientCommunicator.HasErrorOccurred())
                                {
                                    //reload
                                    Global.ClientCommunicator.Reload(ref page);
                                }
                            };
                            RoomsDisplay.Items.Add(btn); //add to layout
                        }
                    }
                }
                NoRoomsLabel.Visibility = Visibility.Hidden; //set as hidden
            }
            else
            {
                RoomsDisplay.Items.Clear();
                NoRoomsLabel.Visibility = Visibility.Visible; //if there are no rooms
            }
            roomlist = newRoomList; //update list to current
        }

        /*
         * room display auto refresher thread.
         * input: none.
         * output: none.
         */
        private void RoomDisplayAutoRefresher(object obj)
        {
            CancellationToken token = (CancellationToken)obj;
            Response.GetRooms newRoomList = null;

            while (!token.IsCancellationRequested)
            {
                newRoomList = Global.ClientCommunicator.GetRooms();
                if (Global.ClientCommunicator.HasErrorOccurred())
                {
                    Dispatcher.Invoke(() =>
                    {
                        cancellationTrigger.Cancel(); //terminate this thread
                    });
                    //reload
                    var page = (Page)this;
                    Global.ClientCommunicator.Reload(ref page);
                }
                else
                {
                    Dispatcher.Invoke(() => SetRoomDisplay(ref newRoomList)); //set new display
                }
                Thread.Sleep(300); //sleep
            }
        }

        /* 
         * OkayButton click event.
         */
        private void OkayButton_Click(object sender, RoutedEventArgs e)
        {
            ErrorGrid.Visibility = Visibility.Hidden;
        }
    }
}
